<?php
App::uses('CakeEmail', 'Network/Email');

class ArCodesController extends AppController {

	//public $components = array('Cookie');

	private	$title_for_layout = 'Lucky Draw';
	private	$keywords = 'Lucky Draw';
	private	$keywordsTh = 'Lucky Draw';
	private	$description = 'Lucky Draw';

	public $uses = array('User','UserGift','Gift');

	public $layout = 'mains';

	//private $ck_cookie;

	public function beforeFilter() 
	{
		/*session_cache_limiter('private');
		$cache_limiter = session_cache_limiter();
		session_cache_expire(365*(60*60*24));
		$cache_expire = session_cache_expire();
		//session_start();*/
		ini_set('session.gc_maxlifetime', 365*(60*60*24)); 
		
		$this->set(array('title_for_layout' => $this->title_for_layout,
						'keywords' => $this->keywords,
						'keywordsTh' => $this->keywordsTh,
						'description' => $this->description
						));

		date_default_timezone_set('Asia/Bangkok');
		
		$this->Session->write('c_user_id',1);
		//debug($this->Session->read('c_user_id'));
			
		if($this->action != 'logout')
		{

			$cUserId = $this->Session->read('c_user_id');
			if($cUserId == null)
			{
				if($this->action != 'index')
				{
					$this->Session->write('cur_page',$this->here);
					$this->redirect(array('controller'=>'arCodes','action' => 'index'));
				}					
			}
			$objUser = $this->User->findById($cUserId);
			$this->set(array('objUser'=>$objUser));		
		}
		
	} // beforeFilter
	

	public function afterFilter()
	{
		$alertType = $this->Session->read('alertType');
	
		if(isset($alertType)){
			$this->Session->delete('alertType');
		}

	} // afterFilter

/*
	public function login() {		
		//$this->layout = 'login';		
			
		if($this->request->is('post'))
		{
			$UserName = $this->User->find('first', array(
				'conditions' => array(
					'username' => $this->request->data['UserName'],
					'password' => $this->request->data['Password'])
			));			
			
			
			if (count($UserName) > 0) {
				//date_default_timezone_set('Asia/Bangkok');			
				//$this->Cookie->write('cookie_login_fb', $UserName);	
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('ชื่อ หรือ รหัสผ่าน ไม่ถูกต้อง!!!');
				$this->redirect(array('action' => 'login'));
			}
		}
	}	
*/
	public function logout()
	{
		$this->Session->delete('c_user_id');
		$this->Session->delete('cur_page');
		/*
		$this->Session->write('alertType','success');
		$this->Session->setFlash('ออกจากระบบเรียบร้อยแล้ว !!!');
		*/
		$this->redirect(array('controller' => 'ArCodes','action' => 'index'));
	}


	public function index()
	{		
		//$cUserId = $this->Cookie->read('c_user_id');
		$cUserId = $this->Session->read('c_user_id');
		$curPage = $this->Session->read('cur_page');
		
		$objUser = $this->User->findById($cUserId);
		$this->set(array('objUser'=>$objUser));
		
		if($this->request->is('post'))
		{
			/*debug($this->request->data);
			exit();*/
			$userF = $this->User->find('first',array('conditions' => array('facebook_id'=>$this->request->data['facebook_id'],'active' => 1)));
			/*debug($userF);
			exit();*/
			if(count($userF) > 0)
			{
				//$this->Cookie->write('c_user_id', $userF["User"]["id"], 365*(60*60*24));	
				$this->Session->write('c_user_id', $userF["User"]["id"]);
				if($curPage != null)
				{
					$this->redirect($curPage);
				}
				else
				{
					$this->redirect(array('controller' => 'users','action' => 'map'));
				}			
			}
			else
			{
				$strPicture = "https://graph.facebook.com/".$this->request->data['facebook_id']."/picture?type=large";
				$strLink = "https://www.facebook.com/app_scoped_user_id/".$this->request->data['facebook_id']."/";	
				$newUserF = $this->User->findById(0);
				$newUserF["User"]["facebook_id"] = $this->request->data['facebook_id'];
				$newUserF["User"]["name"] = $this->request->data['name'];
				$newUserF["User"]["email"] = $this->request->data['email'];
				$newUserF["User"]["picture"] = trim($strPicture);
				$newUserF["User"]["link"] = trim($strLink);
				$newUserF["User"]["phone"] = "";
				$newUserF["User"]["ip"] = $this->request->ClientIp();
				$newUserF["User"]["active"] = 0;
				
				if($this->User->save($newUserF))
				{				
					$newUserF["User"]["id"] = $this->User->getLastInsertId();
					//$this->Cookie->write('c_user_id', $newUserF["User"]["id"], 365*(60*60*24));
					$this->Session->write('c_user_id', $newUserF["User"]["id"]);
					$this->redirect(array('controller' => 'ArCodes','action' => 'index'));					
				}			
			}
		}

		if(count($objUser)> 0)
		{
			if($objUser["User"]["active"] == 0)
			{
				$this->redirect(array('action' => 'add_phone',$objUser["User"]["id"]));
			}
			else
			{
				if($curPage != null)
				{
					$this->redirect($curPage);
				}
				else
				{				
					$this->redirect(array('controller' => 'users','action' => 'map'));
				}
			}
		}
		
		//$this->layout = 'mains';	
	} // index

	public function detail($room_id = null,$local = 'th')
	{

		


	} // detail

/*
	public function login_facebook()
	{
		if($this->request->is('post'))
		{
			//debug($this->request->data);
			//$userF = $this->UserFacebook->find('all',array('conditions' => array('facebook_id'=>$this->request->data['UserFacebook']["facebook_id"])));
			$userF = $this->UserFacebook->find('all',array('conditions' => array('facebook_id'=>$this->request->data['facebook_id'])));
			if($userF)
			{
				$this->Cookie->write('cookie_login_fb', $userF);	
				$this->redirect(array('action' => 'index'));
			}
			else
			{			
				$strPicture = "https://graph.facebook.com/".$this->request->data['facebook_id']."/picture?type=large";
				$strLink = "https://www.facebook.com/app_scoped_user_id/".$this->request->data['facebook_id']."/";	
				$newUserF = $this->UserFacebook->findById(0);
				$newUserF["UserFacebook"]["facebook_id"] = $this->request->data['facebook_id'];
				$newUserF["UserFacebook"]["name"] = $this->request->data['name'];
				$newUserF["UserFacebook"]["email"] = $this->request->data['email'];
				$newUserF["UserFacebook"]["phone"] = "-";
				$newUserF["UserFacebook"]["picture"] = trim($strPicture);
				$newUserF["UserFacebook"]["link"] = trim($strLink);
				
				$this->request->data["UserFacebook"]["picture"] = trim($strPicture);
				$this->request->data["UserFacebook"]["link"] = trim($strLink);
				
				// Create New ID
				
				$strPicture = "https://graph.facebook.com/".$_POST["hdnFbID"]."/picture?type=large";
				$strLink = "https://www.facebook.com/app_scoped_user_id/".$_POST["hdnFbID"]."/";			
				$newUserF = $this->UserFacebook->findById(0);
				$newUserF["UserFacebook"]["facebook_id"] = trim($_POST["hdnFbID"]);
				$newUserF["UserFacebook"]["name"] = trim($_POST["hdnName"]);
				$newUserF["UserFacebook"]["email"] = trim($_POST["hdnEmail"]);
				$newUserF["UserFacebook"]["picture"] = trim($strPicture);
				$newUserF["UserFacebook"]["link"] = trim($strLink);
				
				
				if($this->UserFacebook->save($newUserF))
				{
					$id=$this->UserFacebook->getLastInsertId();
					$this->redirect(array('action' => 'add_phone',$id));
				}
			}
		}
	}
*/	
	public function add_phone($id)
	{
		if($this->request->data)
		{
			$userP = $this->User->find('first',array('conditions' => array('phone'=>$this->request->data['User']['phone'],'id !='=>$id)));
			
			if(count($userP) > 0)
			{
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('มีเบอร์โทรนี้ในระบบแล้ว');
				$this->redirect(array('action' => 'add_phone',$id));
			}
			else
			{
				$this->request->data['User']['id'] = $id;
				$this->request->data['User']['active'] = 1;
				if($this->User->save($this->request->data))
				{				
					$this->redirect(array('controller' => 'users','action' => 'map'));
				}
			}
		}
		else
		{
			$this->request->data = $this->User->findById($id);
			if(!$this->request->data){
				$this->redirect(array('action' => 'index'));
			}
		}		
	}

} 
