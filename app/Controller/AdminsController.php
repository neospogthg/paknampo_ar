<?php
App::uses('CakeEmail', 'Network/Email');

class AdminsController  extends AppController {

	public $components = array('Cookie');

	private	$title_for_layout = 'Lucky Draw';
	private	$keywords = 'Lucky Draw';
	private	$keywordsTh = 'Lucky Draw';
	private	$description = 'Lucky Draw';

	public $uses = array('Gift','GiftType','User','Prefix','TextGift','Privilege','UserGift','CheckinPhoto');

	public $layout = 'mains';

	private $ck_cookie;

	public function beforeFilter() 
	{
		$this->set(array('title_for_layout' => $this->title_for_layout,
						'keywords' => $this->keywords,
						'keywordsTh' => $this->keywordsTh,
						'description' => $this->description
						));

		date_default_timezone_set('Asia/Bangkok');

		$isAdmin = $this->Session->read('isAdmin');	
		if($isAdmin != 1)
		{
			//$this->redirect(array('controller'=>'admins','action' => 'index'));
			if($this->action != 'login')
			{
				$this->redirect(array('controller'=>'admins','action' => 'login'));
			}					
		}
/*
		$this->ck_cookie = $this->Cookie->read('check_cookie');


		if(count($this->ck_cookie)<1){
			$query_cookie = $this->MaehiaEbookingCookie->find('first', array('conditions' => array(),'order' => 'MaehiaEbookingCookie.id DESC'));

			$check_cookie = $query_cookie['MaehiaEbookingCookie']['id']+1;
			//debug($ck_booking);
			$this->Cookie->write('check_cookie',$check_cookie);
			$this->request->data['MaehiaEbookingCookie']['ip'] = $this->request->ClientIp();
			$this->MaehiaEbookingCookie->save($this->request->data);
			
			$this->redirect(array('action' => 'index'));
		}
		*/

	} // beforeFilter

	public function afterFilter()
	{
		$alertType = $this->Session->read('alertType');
	
		if(isset($alertType)){
			$this->Session->delete('alertType');
		}

	} // afterFilter

	public function login() {		
		if($this->request->is('post'))
		{
			if($this->request->data['UserName'] == "admin" && $this->request->data['Password'] =="pnp1277")
			{
				$this->Session->write('isAdmin', 1);	
				$this->redirect(array('action' => 'gift_list'));
			}
			else
			{
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('ชื่อ หรือ รหัสผ่าน ไม่ถูกต้อง!!!');
				$this->redirect(array('action' => 'login'));
			}
		}
	}	
	
	public function logout()
	{
		//$this->Session->delete('admin');
		$this->Session->delete('isAdmin');
		$this->Session->write('alertType','success');
		$this->Session->setFlash('ออกจากระบบเรียบร้อยแล้ว !!!');
		$this->redirect(array('action' => 'login'));

	}


	public function index()
	{
		$this->redirect(array('action' => 'gift_list'));
	}

	public function gift_list()
	{
		$gifts = $this->Gift->find('all', array('conditions' => array('active' => 1),'order' => 'Gift.id ASC'));
		$this->set(array('gifts'=>$gifts));		
		
	}

	public function add_gift()
	{	
		$giftTypes = $this->GiftType->find('list');
		$this->set(array('giftTypes'=>$giftTypes));		

		if($this->request->is('post'))
		{
			
			if($this->Gift->save($this->request->data))
			{
				$id=$this->Gift->getLastInsertId();
				$this->request->data['Gift']['id'] = $id;
				
				if(!empty($this->request->data['Gift']['photo_tmp']['name']))
				{
					$folderToSaveFiles = WWW_ROOT . 'img/gift_pic/';
					$file = $this->data['Gift']['photo_tmp'];
					$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png');
					if(in_array($ext, $arr_ext))
					{
						$newName = 'G'.$id.'.'.$arr_ext[0];
						if(move_uploaded_file($file['tmp_name'], $folderToSaveFiles . $newName))
						{		
							$this->request->data['Gift']['photo'] = $newName;
							$this->Gift->save($this->request->data);
						}
					}
				}
								
				$this->Session->write('alertType','success');
				$this->Session->setFlash('Save Complete');
				$this->redirect(array('action' => 'gift_list'));
				
			}
		
		}
		
	}
	public function edit_gift($id=null)
	{	
		$giftTypes = $this->GiftType->find('list');
		$this->set(array('giftTypes'=>$giftTypes));
		
		if($this->request->data)
		{	
			$this->request->data['Gift']['id'] = $id;
			if($this->Gift->save($this->request->data))
			{				
				if(!empty($this->request->data['Gift']['photo_tmp']['name']))
				{
					$folderToSaveFiles = WWW_ROOT . 'img/gift_pic/';
					$file = $this->data['Gift']['photo_tmp'];
					$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png');
					if(in_array($ext, $arr_ext))
					{
						$newName = 'G'.$id.'.'.$arr_ext[0];
						if(move_uploaded_file($file['tmp_name'], $folderToSaveFiles . $newName))
						{		
							$this->request->data['Gift']['photo'] = $newName;
							$this->Gift->save($this->request->data);
						}
					}
				}
								
				$this->Session->write('alertType','success');
				$this->Session->setFlash('Save Complete');
				$this->redirect(array('action' => 'edit_gift',$id));
			}				
		}
		else
		{
			$this->request->data = $this->Gift->findById($id);
			if(!$this->request->data){
				$this->redirect(array('action' => 'gift_list'));
			}
		}		
		
	}
	public function delete_gift($id=null)
	{
		$gift = $this->Gift->findById($id);
		$gift['Gift']['active'] = 0;
		
		if($this->Gift->save($gift))
		{		
			$this->redirect(array('action' => 'gift_list'));
		}		
	}		


	public function user_list()
	{
		$users = $this->User->find('all', array('order' => 'User.id ASC'));
		$this->set(array('users'=>$users));		
		
	}
	
	public function delete_user($id=null)
	{
		/*
		$user = $this->User->findById($id);
		$user['User']['active'] = 0;
		
		if($this->User->save($user))
		{		
			$this->redirect(array('action' => 'user_list'));
		}	
		*/
	}

	public function big_gift()
	{
		$gifts = $this->Gift->find('all', array('conditions' => array('active' => 1,'Gift.gift_type_id' => 2),'order' => 'Gift.id ASC'));
		$this->set(array('gifts'=>$gifts));		

		if($this->request->is('post')){
			debug('1');
		}
	} // fn big_gift

	public function gift_random($gift_id = null)
	{

		$this->layout = '';

		$giftTypes = $this->GiftType->find('first', array('conditions' => array('GiftType.id' => $gift_id),'order' => 'GiftType.id ASC'));

		if($gift_id == 2)
		{
			// รางวัลใหญ่ ต้อง CheckIn ให้ ครบ 4 จุด
			/*
			$user = $this->User->find('first',
                       array('conditions'=>array('User.active'=>1,
												 'User.photo1 !='=> null,
												 'User.photo2 !='=> null,
												 'User.photo3 !='=> null,
												 'User.photo4 !='=> null,
												 'User.photo5 !='=> null,
												 'User.biggift_status' => 0
												),
                       'order' => 'rand()'));
			*/	
			$user = $this->Privilege->find('all',
                       array('conditions'=>array('User.active'=>1,'Privilege.active'=>1),
                       'order' => ''));	   
			/*-$user = $this->Privilege->find('first',
                       array('conditions'=>array('User.active'=>1,'Privilege.active'=>1),
                       'order' => 'rand()'));	-*/			
		}
		/*
		else{
			$this->Session->write('alertType','success');
			$this->Session->setFlash('Save Complete');
			$this->redirect(array('action' => 'edit_gift',$id));
		}
		*/
		//debug($user);
		$this->set(compact('giftTypes','user','gift_id'));	


	} // fn gift_random

	public function confrim_gift($user_id = null,$gift_id = null,$privilege_id=null)
	{
		$privilege = $this->Privilege->findById($privilege_id);
		$privilege["Privilege"]["active"] = 0;
		if($this->Privilege->save($privilege))
		{
			$userGift = $this->UserGift->findById(0);
			$userGift["UserGift"]["user_id"] = $user_id;
			$userGift["UserGift"]["gift_id"] = $gift_id;
			$userGift["UserGift"]["privilege_id"] = $privilege_id;	
			$userGift["UserGift"]["active"] = 1;
			if($this->UserGift->save($userGift))
			{
				$this->redirect(array('controller' => 'Admins','action' => 'gift_list'));
			}						
		}
/*
		if($gift_id == 2){
			$this->request->data['User']['id'] = $user_id;
			$this->request->data['User']['biggift_status'] = 1;
			$this->User->save($this->request->data);
			$this->redirect(array('controller' => 'Admins','action' => 'gift_random',$gift_id));
		}elseif($gift_id == 1){
			$this->request->data['User']['id'] = $user_id;
			$this->request->data['User']['smallgift_status'] = 1;
			$this->User->save($this->request->data);
			$this->redirect(array('controller' => 'Admins','action' => 'gift_random',$gift_id));
		}else{
			$this->redirect(array('controller' => 'ArCodes','action' => 'index'));
		}
*/		
	} // fn confrim_gift

	public function add_big_gift($id_gift = null)
	{
		$user = $this->User->find('first',
                       array('conditions'=>array('User.active'=>1,
					   							 'User.photo1 !='=> null,
												 'User.photo2 !='=> null,
												 'User.photo3 !='=> null,
												 'User.photo4 !='=> null,
												 'User.photo5 !='=> null,
												),
                       'order' => 'rand()'));
		//debug($user);
		$this->set(compact('id_gift','user'));
	} // fn big_gift
	
	public function text_gift()
	{	
		if($this->request->data)
		{
			//debug($this->request->data);
			//exit();
			if($this->TextGift->save($this->request->data["TextGift"]))
			{												
				$this->Session->write('alertType','success');
				$this->Session->setFlash('Save Complete');
				$this->redirect(array('action' => 'text_gift'));
			}				
		}
		else
		{
			$this->request->data = $this->TextGift->find("first");
			if(!$this->request->data){
				$this->redirect(array('action' => 'text_gift'));
			}
		}
		
	}

	public function user_checkin($id=null)
	{
		$this->request->data = $this->User->findById($id);
		
		$checkinPhotos = $this->CheckinPhoto->find('all', array(
			'conditions' => array('CheckinPhoto.user_id' => $id,
								  'CheckinPhoto.active' => 1
		)));
		$this->set(array('checkinPhotos'=>$checkinPhotos));		
	}		
} 
