<?php

class UsersController extends AppController 
{
	//public $components = array('Cookie');
	
	private	$title_for_layout = 'Lucky Draw';
	private	$keywords = 'Lucky Draw';
	private	$keywordsTh = 'Lucky Draw';
	private	$description = 'Lucky Draw';
	
	public $uses = array('User','Prefix','UserGift','Gift','Privilege','CheckinPhoto','TextGift');

	public $layout = 'mains';

	private	$url = '/';
	
	//private $ck_cookie;
	
	public function beforeFilter() 
	{		
		$this->set(array('title_for_layout' => $this->title_for_layout,
						'keywords' => $this->keywords,
						'keywordsTh' => $this->keywordsTh,
						'description' => $this->description
						));
						
		date_default_timezone_set('Asia/Bangkok');				
		/*
		date_default_timezone_set('Asia/Bangkok');

		set_time_limit(0);
		ini_set("max_execution_time","0");
		ini_set("memory_limit","128M");
		
		$idCard = $this->Session->read('idCard');

		$arrConditionMain = array('callback','user_info','login','index','search_person','view_person_department_summary','view_person_position_summary','employee_detail','view_person_department','view_person_manager');


		if(!isset($idCard))
		{
			if(in_array($this->action,$arrConditionMain)){
				
			}else{
				$this->redirect(array('controller' => 'Mains','action' => 'callback'));	
			}
		}
		*/
		//$cUserId = $this->Cookie->read('c_user_id');
		
		//session_start();
		
		
		$cUserId = $this->Session->read('c_user_id');
		//debug($this->Session->read('c_user_id'));
		if($cUserId == null)
		{
			if($this->action != 'index')
			{
				$this->Session->write('cur_page',$this->here);
				$this->redirect(array('controller'=>'arCodes','action' => 'index'));
			}					
		}
		
		$objUser = $this->User->findById($cUserId);
		$this->set(array('objUser'=>$objUser));
		
		
	}

	public function afterFilter()
	{
		$alertType = $this->Session->read('alertType');
		if(isset($alertType)){
			$this->Session->delete('alertType');
		}

	}

	public function callback()
	{
		$this->layout = '';
	}


	public function index()
	{
			
	}

	public function map()
	{
		$cUserId = $this->Session->read('c_user_id');
		$objUser = $this->User->findById($cUserId);
		$this->set(array('objUser'=>$objUser));
	
		$checkinPhotos = $this->CheckinPhoto->find('all', array(
			'conditions' => array('CheckinPhoto.user_id' => $cUserId,
								  'CheckinPhoto.active' => 1
		)));
		$this->set(array('checkinPhotos'=>$checkinPhotos));
		
		//debug($this->Session->read('c_user_id'));
		/*
		if($this->request->is('post'))
		{
		}	*/		
	}

	public function giveaway_results()
	{
		$userGifts = $this->UserGift->find("all", array('conditions' => array('UserGift.active' => 1)));
		$this->set(array('userGifts'=>$userGifts));	
			
		/*$textGift = $this->TextGift->find("first");
		$this->set(array('textGift'=>$textGift));	*/
	} // giveaway_results
	
	public function add_user()
	{
		/*$prefixs=$this->Prefix->find('list',array(
					'fields' => array('Prefix.id', 'Prefix.name'),
					'conditions' => null));
		$this->set(array('prefixs'=>$prefixs));*/
		
		//$birthdate = $this->DateThai(date("Y-m-d"));
		//$this->set(array('birthdate'=>$birthdate));
		
		if($this->request->is('post'))
		{
			//$this->request->data['User']['birthdate'] = $this->DateEng($this->request->data['User']['birthdate']);			
			if($this->User->save($this->request->data))
			{
/*	
				$userId=$this->User->getLastInsertId();
				$this->request->data['User']['id'] = $userId;

			
				//Save photo
				if(!empty($this->request->data['User']['photo_tmp']['name']))
				{
					$folderToSaveFiles = WWW_ROOT . 'img/user_profile/';
					$file = $this->data['User']['photo_tmp'];
					$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png');
					if(in_array($ext, $arr_ext))
					{
						$newName = $userId.'.'.$arr_ext[0];
						if(move_uploaded_file($file['tmp_name'], $folderToSaveFiles . $newName))
						{		
							$this->request->data['User']['photo'] = $newName;
							$this->User->save($this->request->data);
						}
					}
				}
*/								
				$this->Session->write('alertType','success');
				$this->Session->setFlash('Save Complete');
				$this->redirect(array('controller' => 'arCodes','action' => 'map'));
			}				
		}
	}
	
	public function edit_user($id=null)
	{
		/*$prefixs=$this->Prefix->find('list',array(
					'fields' => array('Prefix.id', 'Prefix.name'),
					'conditions' => null));
		$this->set(array('prefixs'=>$prefixs));*/
		
		if($this->request->data)
		{
			$this->request->data['User']['id'] = $id;
			//$this->request->data['User']['birthdate'] = $this->DateEng($this->request->data['User']['birthdate']);	
			//debug($this->request->data);
			//exit;
			if($this->User->save($this->request->data))
			{	
				/*
				if(!empty($this->request->data['User']['photo_tmp']['name']))
				{
					$folderToSaveFiles = WWW_ROOT . 'img/user_profile/';
					$file = $this->data['User']['photo_tmp'];
					$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png');
					if(in_array($ext, $arr_ext))
					{
						$newName = $id.'.'.$arr_ext[0];
						if(move_uploaded_file($file['tmp_name'], $folderToSaveFiles . $newName))
						{		
							$this->request->data['User']['photo'] = $newName;
							$this->User->save($this->request->data);
						}
					}
				}
				*/				
				$this->Session->write('alertType','success');
				$this->Session->setFlash('Save Complete');
				$this->redirect(array('action' => 'edit_user',$id));
			}				
		}
		else
		{
			$this->request->data = $this->User->findById($id);
			//$this->request->data['User']['birthdate'] = $this->DateThai($this->request->data['User']['birthdate']);
			if(!$this->request->data){
				$this->redirect(array('action' => 'map'));
			}
		}
	}

	public function ar_code_scan($id=null)
	{

	} // ar_code_scan

	public function receive($id=null)
	{
		
	} 	

	public function save_photo($checkin_name=null)
	{
		//$this->layout = '';
	//	$cUserId = $this->Cookie->read('c_user_id');
		$this->Session->delete('cur_page');
		
		if($checkin_name == 'Marker01'){
			$checkin_id = 1;
		}elseif($checkin_name == 'Marker02'){
			$checkin_id = 2;
		}elseif($checkin_name == 'Marker03'){
			$checkin_id = 3;
		}elseif($checkin_name == 'Marker04'){
			$checkin_id = 4;
		}elseif($checkin_name == 'Marker05'){
			$checkin_id = 5;
		}else{
			$this->redirect(array('action' => 'map'));
			//$checkin_id = 0;
			//$this->redirect(array('action' => 'index'));
		}

		$this->set(compact('checkin_name'));
	
		if($this->request->is('post'))
		{
			$cUserId = $this->User->findById($this->Session->read('c_user_id'));
			
			$start_date = new DateTime();
			$start_date->setTime(0,0);
			$end_date = new DateTime();
			$end_date->setTime(23,59,59);
			$countPrivilege = $this->Privilege->find('count', array(
				'conditions' => array('Privilege.user_id' => $cUserId['User']['id'],
									  'Privilege.active' => 1,
									  'Privilege.created >' => $start_date->format('Y-m-d H:i:s'),
									  'Privilege.created <' => $end_date->format('Y-m-d H:i:s')
			)));
			
			if($countPrivilege == 0)
			{
				$countCheckin = $this->CheckinPhoto->find('count', array(
					'conditions' => array('CheckinPhoto.user_id' => $cUserId['User']['id'],
									      'CheckinPhoto.checkin_id' => $checkin_id,
										  'CheckinPhoto.active' => 1,
										  'CheckinPhoto.is_finish' => 0
				)));
				
				if($countCheckin ==0)
				{
					$file = $this->request->data['File']['files_old'];

					$folderToSaveFiles = WWW_ROOT . 'img/checkin_photo'.$checkin_id.'/';

					$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
					$result = move_uploaded_file( $file['tmp_name'], $folderToSaveFiles.$file['name']);
					//$result = move_uploaded_file( $file['tmp_name'], $folderToSaveFiles . $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext);
					
					/*
					$this->request->data['User']['id'] = $cUserId['User']['id'];

					if($checkin_id == 1){
						$this->request->data['User']['photo1'] = $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext;
					}elseif($checkin_id == 2){
						$this->request->data['User']['photo2'] = $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext;
					}elseif($checkin_id == 3){
						$this->request->data['User']['photo3'] = $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext;
					}elseif($checkin_id == 4){
						$this->request->data['User']['photo4'] = $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext;
					}elseif($checkin_id == 5){
						$this->request->data['User']['photo5'] = $cUserId['User']['id'].'_'.$checkin_id.'.'.$ext;
					}else{
						$this->redirect(array('action' => 'map'));
					}
					$this->User->clear();
					$this->User->save($this->request->data);
					*/
					
					if($result)
					{
						$checkinPhoto = $this->CheckinPhoto->findById(0);
						$checkinPhoto['CheckinPhoto']['user_id'] = $cUserId['User']['id'];		
						$checkinPhoto['CheckinPhoto']['checkin_id'] = $checkin_id;
						$checkinPhoto['CheckinPhoto']['active'] = 1;
						$checkinPhoto['CheckinPhoto']['is_finish'] = 0;
						$checkinPhoto['CheckinPhoto']['photo'] = $file['name'];				
						if($this->CheckinPhoto->save($checkinPhoto))
						{
							$countPhoto = $this->CheckinPhoto->find('count', array(
								'fields' => 'DISTINCT CheckinPhoto.checkin_id',
								'conditions' => array('CheckinPhoto.user_id' => $cUserId['User']['id'],
													  'CheckinPhoto.active' => 1,
													  'CheckinPhoto.is_finish' => 0
							)));					
							if($countPhoto >= 5)
							{					
								if($countPrivilege == 0)
								{
									$privilege = $this->Privilege->findById(0);
									$privilege['Privilege']['user_id'] = $cUserId['User']['id'];
									$privilege['Privilege']['active'] = 1;
									if($this->Privilege->save($privilege))
									{
										$this->CheckinPhoto->updateAll(array('is_finish' => 1), array('user_id' => $cUserId['User']['id']));																				
									}
								}				
							}	
							
							$checkin_photo_id = $this->CheckinPhoto->getLastInsertId();
							$this->redirect(array('action' => 'my_photo_checkin',$checkin_photo_id));								
						}				
					}
				}
				else
				{
					$this->redirect(array('action' => 'map'));
				}
			}
			else
			{
				$this->redirect(array('action' => 'map'));
			}
		}

	}// fn save_photo

	public function my_photo_checkin($checkin_photo_id=null)
	{
		$checkinPhoto = $this->CheckinPhoto->findById($checkin_photo_id);
		$checkin_id = $checkinPhoto["CheckinPhoto"]["checkin_id"];
		
		if($checkin_id == 1){
			$checkin_name = 'Marker01';
		}elseif($checkin_id == 2){
			$checkin_name = 'Marker02';
		}elseif($checkin_id == 3){
			$checkin_name = 'Marker03';
		}elseif($checkin_id == 4){
			$checkin_name = 'Marker04';
		}elseif($checkin_id == 5){
			$checkin_name = 'Marker05';
		}else{
			$this->redirect(array('action' => 'map'));
		}

		$cUserId = $this->User->findById($checkinPhoto['CheckinPhoto']['user_id']);
		$this->set(compact('cUserId','checkin_id','checkin_name','checkinPhoto'));

	}// fn save_photo

	public function share_facebook($checkin_id=null)
	{
		$this->layout = '';
	}// fn share_facebook



	
} // End Class
