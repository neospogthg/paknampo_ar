<!DOCTYPE html>
<html lang="en">
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta content="" name="author" />
		<meta content="<?php echo $keywords; ?>" name="keywords" />
		<meta content="<?php echo $keywordsTh; ?>" name="keywords" lang="th"/>
		<meta content="<?php echo $description; ?>" name="description" />
		
		<?php
			echo $this->Html->css('blue_ui');		
			//echo $this->Html->meta('favicon.ico','/img/Logo_ENG.png',array('type' => 'icon'));

			//echo $this->Html->css('cake.generic');	
			//echo $this->Html->css('paknampho');	
			echo $this->Html->css('navbar');
			echo $this->Html->css('fadeText');
			echo $this->Html->css('slide_navbar');
		?>

		<title>
			<?php echo $title_for_layout; ?>
		</title>

		<?php echo $this->Html->css('/assets/font-awesome-4.7.0/css/font-awesome.min'); ?>
		<!-- Bootstrap core CSS -->
		<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>
		
		<!-- Custom styles for this template -->
		<?php echo $this->Html->css('/assets/bootstrap/sticky-footer-navbar'); ?>

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
			<?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
		<![endif]-->

		<!-- <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet"> -->
		<?php 
		echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min');
		echo $this->Html->script('jquery.floatThead.min'); 	
		?>
		<?php //echo $this->Html->script('mdb'); ?>
		<?php echo $this->Html->script('jquery.dataTables.min'); ?>
		<?php echo $this->Html->script('dataTables.bootstrap.min'); ?>
		<!-- ckeditor -->
		<?php echo $this->Html->script('ckeditor/ckeditor');?>

		<?php
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>

		<?php //echo $this->Html->css('/assets/bootstrap-datepicker-custom/css/bootstrap-datepicker3.min'); ?>	
		<?php //echo $this->Html->script('/assets/bootstrap-datepicker-custom/js/bootstrap-datepicker-custom'); ?>
		<?php //echo $this->Html->script('/assets/bootstrap-datepicker-custom/locales/bootstrap-datepicker.th.min'); ?>
		
		<?php echo $this->Html->css('/assets/DatepickerThai/css/datepicker.css'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.js'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.th.js'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker-thai.js'); ?>
		
	</head>

<?php 
/*-
echo $this->Html->script('/assets/charts/jquery.jqplot.js'); 
echo $this->Html->script('/assets/charts/plugins/jqplot.barRenderer.js'); 
echo $this->Html->script('/assets/charts/plugins/jqplot.pieRenderer.js'); 
echo $this->Html->script('/assets/charts/plugins/jqplot.categoryAxisRenderer.js'); 
echo $this->Html->script('/assets/charts/plugins/jqplot.pointLabels.js'); 
echo $this->Html->css('/assets/charts/jquery.jqplot.css'); 
-*/
?>

<!-- DATA TABLE -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<body style="<?php echo $this->Html->style(array('margin-bottom' => '0'));?>;">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54232567-2', 'auto');
  ga('send', 'pageview');

</script>


<?php 	
	
	$alert = $this->Session->flash();
	
	if($alert){
		if(!isset($alertType)){
			$alertType = $this->Session->read('alertType');
		}
?>

	<div class="row">
		<div class="alert alert-<?php echo $alertType; ?>" role="alert" style="text-align: center;">
			<?php echo $alert; ?>
		</div>
	</div>
	
<?php 		
	} 
?>

  <div id="wrapper" class="" >
         <div class="overlay" style="display: none;" style="background-color: red"></div>
         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation" >
            <ul class="nav sidebar-nav">
               <li class="sidebar-brand">
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"> BLESM </a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-camera"></i> Photo</a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-facetime-video"></i> Video</a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-headphones"></i> Music</a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-cloud"></i> Cloud</a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-th"></i> Apps</a>
               </li>
               <li>
                  <a href="https://github.com/FragCoder/bootstrap-left-slide-menu"><i class="glyphicon glyphicon-cog"></i> Settings</a>
               </li>
            </ul>
         </nav>
         <div id="page-content-wrapper">
            <button type="button" class="hamburger animated fadeInLeft is-closed" data-toggle="offcanvas">
            <span class="hamb-top" style="background-color: #0071fd"></span>
            <span class="hamb-middle" style="background-color: #0071fd"></span>
            <span class="hamb-bottom" style="background-color: #0071fd"></span>
            </button>
            <div class="container-fluid" style="margin-top:-70px">
		
		


</div>	
            
               <?php echo $this->fetch('content'); ?>
            
         </div>
      </div>







<script type="text/javascript">
	$(document).ready(function () {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
       isClosed = false;

    function buttonSwitch() {

        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    trigger.click(function () {
        buttonSwitch();
    });

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });
});
</script>





    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<?php //echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'); ?>
	<?php 
		//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
		echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
	?>
    
	<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie10-viewport-bug-workaround'); ?>

	
	<?php //echo $this->element('sql_dump'); ?>

	<!-- Script2 for finding in web-->
	<?php echo $this->Html->script('/assets/select2/dist/js/select2.min'); ?>


	<script type="text/javascript">
		$(document).ready(function() {
		  $(".js-example-basic-single").select2();
		});

	</script> 
	<script>
		$(document).ready(function(){
		  $('.dropdown-submenu a.test').on("click", function(e){
		    $(this).next('ul').toggle();
		    e.stopPropagation();
		    e.preventDefault();
		  });
		});

		(function($){
			$(document).ready(function(){
				$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
					event.preventDefault(); 
					event.stopPropagation(); 
					$(this).parent().siblings().removeClass('open');
					$(this).parent().toggleClass('open');
				});
			});
		})(jQuery);

	</script>





  </body>
</html>
