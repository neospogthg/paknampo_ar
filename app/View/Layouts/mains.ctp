<!DOCTYPE html>
<html lang="en">
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta content="" name="author" />
		<meta content="<?php echo $keywords; ?>" name="keywords" />
		<meta content="<?php echo $keywordsTh; ?>" name="keywords" lang="th"/>
		<meta content="<?php echo $description; ?>" name="description" />
		
		<?php
			echo $this->Html->css('blue-ui');		
			//echo $this->Html->meta('favicon.ico','/img/Logo_ENG.png',array('type' => 'icon'));

			//echo $this->Html->css('cake.generic');	
			//echo $this->Html->css('paknampho');	
			echo $this->Html->css('navbar');
		?>

		<title>
			<?php echo $title_for_layout; ?>
		</title>

		<?php echo $this->Html->css('/assets/font-awesome-4.7.0/css/font-awesome.min'); ?>
		<!-- Bootstrap core CSS -->
		<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>
		
		<!-- Custom styles for this template -->
		<?php echo $this->Html->css('/assets/bootstrap/sticky-footer-navbar'); ?>

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
			<?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
		<![endif]-->

		<!-- <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet"> -->
		<?php 
		echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min');
		echo $this->Html->script('jquery.floatThead.min'); 	
		?>
		<?php //echo $this->Html->script('mdb'); ?>
		<?php echo $this->Html->script('jquery.dataTables.min'); ?>
		<?php echo $this->Html->script('dataTables.bootstrap.min'); ?>
		<!-- ckeditor -->
		<?php echo $this->Html->script('ckeditor/ckeditor');?>

		<?php
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
		
		<?php echo $this->Html->css('/assets/DatepickerThai/css/datepicker.css'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.js'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.th.js'); ?>
		<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker-thai.js'); ?>

	</head>

<!-- DATA TABLE -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<body style="<?php echo $this->Html->style(array('margin-bottom' => '0'));?> ; background-image: url('<?php echo $this->Html->url('/img/body_03.png'); ?>');background-size: 100% 100%;background-repeat: no-repeat;margin-bottom: 1px">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54232567-2', 'auto');
  ga('send', 'pageview');

</script>
			   
<?php
	$isAdmin = $this->Session->read('isAdmin');	
	
	if((isset($objUser) && count($objUser) > 0) || ($isAdmin == 1))
	{
			
?>
<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-default"
               style="<?php echo $this->Html->style(array('min-height' => 'auto','margin-bottom' => '0','border' => '0','border-radius' => '0'));?>
			   background-color:#eede90;">
				<div class="container">
					<div class="navbar-header">

						<a class="navbar-brand" id="scal-phone-header" href="<?php echo $this->Html->url(array('controller' => 'arCodes','action' => 'index')); ?>" style="<?php echo $this->Html->style(array('width' => '15%'));?>">
							<img src="<?php echo $this->Html->url('/img/Asset 5@10x.png'); ?>"  id="logo" class="img-responsive" />
						</a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

					</div>


					<div id="navbar" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right navbar-nav2">
							<!--li>
								<a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "gift_list")); ?>"><i class="fa fa-align-center" aria-hidden="true"></i> เกี่ยวกับ</a>
						   </li>
							<li>
								<a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "gift_list")); ?>"><i class="fa fa-align-center" aria-hidden="true"></i>ติดต่อ</a>
						   </li-->		
						   <!--li>
								สวัสดีคุณ : <?php echo($objUser["User"]["name"]);?>
						   						</li-->

<?php
		if(isset($objUser) && count($objUser) > 0)
		{
?>							
						   <li>
								<a href="<?php echo $this->Html->url(array("controller" => "Users","action" => "edit_user",$objUser["User"]["id"])); ?>">
<?php						   
			if($objUser['User']['picture'] != "")
			{
				echo("<img src=".$objUser['User']['picture']." style='width:25px;height:25px;border-radius:50%;'/>");										
			}
				else
			{
				echo("<i class='glyphicon glyphicon-user'></i>");
			}
?>										
										
									ข้อมูลส่วนตัว
								</a>
						   </li>			   
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Users","action" => "map")); ?>"><i class="fa fa-map-marker" aria-hidden="true"></i> แผนที่งาน</a>
						   </li>
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Users","action" => "giveaway_results")); ?>"><i class="fa fa-bell" aria-hidden="true"></i> ผลการแจกรางวัล</a>
						   </li>
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "ArCodes","action" => "logout")); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</a>
						   </li>
						   
<?php
		}
		if($isAdmin	== 1)
		{
?>
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "gift_list")); ?>"><i class="fa fa-align-center" aria-hidden="true"></i> สุ่มรางวัล</a>
						   </li>
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "user_list")); ?>"><i class="fa fa-align-center" aria-hidden="true"></i> ผู้ใช้งาน</a>
						   </li>
						   <!--li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "text_gift")); ?>"><i class="fa fa-align-center" aria-hidden="true"></i> ประกาศผลรางวัล</a>
						   </li-->						   
						   <li>
							  <a href="<?php echo $this->Html->url(array("controller" => "Admins","action" => "logout")); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</a>
						   </li>
<?php						   
		}	
?>	
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
		</div>	

		<div class="row">
			<div class="container-fulid">
				<img src="<?php echo $this->Html->url('/img/header_03.png'); ?>" style="width:100%;margin-left:-1px"/>
			</div>
		</div>		
<?php
	}
	else
	{
?>
<div class="container-fulid" >

	<img src="<?php echo $this->Html->url('/img/header_03.png'); ?>" style="width:100%;margin-left:-1px"/>

</div>
<?php
	}
?> 		
<div class="brnav"></div>		

<?php 	
	
	$alert = $this->Session->flash();
	
	if($alert){
		if(!isset($alertType)){
			$alertType = $this->Session->read('alertType');
		}
?>

	<div class="row">
		<div class="alert alert-<?php echo $alertType; ?>" role="alert" style="text-align: center;">
			<?php echo $alert; ?>
		</div>
	</div>
	
<?php 		
	} 
?>

</div>	

	<?php echo $this->fetch('content'); ?>

<div class="container-fulid">

	<img src="<?php echo $this->Html->url('/img/footer_03.png');?>" style="width:100%;"/>

</div>
<!--footer class="footer" style="background-color:#333;">
<div class="container-fluid">
</div>
</footer-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<?php //echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'); ?>
	<?php 
		//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
		echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
	?>
    
	<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie10-viewport-bug-workaround'); ?>

	
	<?php //echo $this->element('sql_dump'); ?>

	<!-- Script2 for finding in web-->
	<?php echo $this->Html->script('/assets/select2/dist/js/select2.min'); ?>

	<script type="text/javascript">
		$(document).ready(function() {
		  $(".js-example-basic-single").select2();
		});

	</script> 
	<script>
		$(document).ready(function(){
		  $('.dropdown-submenu a.test').on("click", function(e){
		    $(this).next('ul').toggle();
		    e.stopPropagation();
		    e.preventDefault();
		  });
		});

		(function($){
			$(document).ready(function(){
				$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
					event.preventDefault(); 
					event.stopPropagation(); 
					$(this).parent().siblings().removeClass('open');
					$(this).parent().toggleClass('open');
				});
			});
		})(jQuery);

	</script>



  </body>
</html>
