<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>


<div class="container">
	<div class="col-md-12">
		<div class="container">
			<div class="row">			
				<?php echo $this->Form->create('UserFacebook',array('name'=>'UserFacebook','id'=>'UserFacebook','class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>
				<?php echo $this->Form->input('facebook_id', array('id'=>'facebook_id','name'=>'facebook_id','type'=>'hidden')); ?>
				<?php echo $this->Form->input('name', array('id'=>'name','name'=>'name','type'=>'hidden')); ?>
				<?php echo $this->Form->input('email', array('id'=>'email','name'=>'email','type'=>'hidden')); ?>	

					
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v5.0"></script>
<div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false" onlogin="checkLoginState();"></div>

			</div>
		</div>
	</div>
</div>

<script>

  var bFbStatus = false;
  var fbID = "";
  var fbName = "";
  var fbEmail = "";

  window.fbAsyncInit = function() {
    FB.init({
      appId            : '426465808059089',
      autoLogAppEvents : true,
      xfbml            : true,
	version          : 'v5.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


function statusChangeCallback(response)
{
		if(bFbStatus == false)
		{
			fbID = response.authResponse.userID;

			  if (response.status == 'connected') {
				getCurrentUserInfo(response)
			  } else {
				FB.login(function(response) {
				  if (response.authResponse){
					getCurrentUserInfo(response)
				  } else {
					console.log('Auth cancelled.')
				  }
				}, { scope: 'email' });
			  }
		}


		bFbStatus = true;
}


    function getCurrentUserInfo() {
      FB.api('/me?fields=name,email', function(userInfo) {

		  fbName = userInfo.name;
		  fbEmail = userInfo.email;

			$("#facebook_id").val(fbID);
			$("#name ").val(fbName);
			$("#email").val(fbEmail);
			$("#UserFacebook").submit();

      });
    }

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

//setInterval(function(){ checkLoginState(); }, 2000);

</script>