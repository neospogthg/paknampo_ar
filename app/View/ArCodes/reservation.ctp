<style type="text/css">
    #invoice{
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }

    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #3989c6;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }
</style>



<!--Author      : @arboshiki-->
<div id="invoice">

    <div class="toolbar hidden-print">
        <!--<div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
        </div>
        <hr>-->
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a target="_blank" href="https://www.agri.cmu.ac.th.com">
                            <img src="<?php echo $this->Html->url('/img/logo.png'); ?>" data-holder-rendered="true" />
                            </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a target="_blank" href="http://www.agri.cmu.ac.th">
                            คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่
                            </a>
                        </h2>
                        <div>155 หมู่ 2 ต.แม่เหียะ อ.เมือง จ.เชียงใหม่</div>
                        <div>053-948401 ต่อ 2</div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to"><?php echo $user['User']['name']; ?></h2>
                        <div class="tel"><i class="fa fa-mobile" aria-hidden="true"></i> <?php echo $user['User']['tel']; ?></div>
                        <div class="email"><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:<?php echo $user['User']['email']; ?>"><?php echo $user['User']['email']; ?></a></div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id"><?php echo $user['Room']['name']; ?></h1>
                        <h1 class="invoice-id">INVOICE : <?php echo $user['User']['code']; ?></h1>
                        <div class="date">วันที่ของใบแจ้งหนี้: <?php echo $user['User']['created']; ?></div>
                        <?php 
                            $new_startDate = date("Y-m-d",strtotime($user['User']['created'])); 
                            //echo 'THIS => '. $new_startDate; 
                        ?>
                        <!--<div class="date">วันที่ครบกำหนด : <?php //echo $strNewDate = date("Y-m-d", strtotime("+7 day", strtotime($new_startDate))); ?></div>-->
                    </div>
                </div>
                <?php

                        $date_range = $user['User']['date_range'];
                        $str=explode("-",$date_range);

                        $str_start=explode("/",$str[0]);
                        $str_end=explode("/",$str[1]);

                        $start_date = trim($str_start[2]).'/'.trim($str_start[1]).'/'.trim($str_start[0]);
                        $end_date = trim($str_end[2]).'/'.trim($str_end[1]).'/'.trim($str_end[0]);

                        function DateDiff($strDate1,$strDate2)
                        {
                            return (strtotime($strDate2) - strtotime($strDate1))/ ( 60 * 60 * 24 );  // 1 day = 60*60*24
                        }

                        $count_date = DateDiff($start_date,$end_date)+1;

                            //SET VALUE
                                $total_row = 0;
                                $total_all = 0;
                                $no_i = 2;

                ?>
                <div><b>ใช้บริการตั้งแต่วันที่ : </b> <?php echo $user['User']['date_range'].' ('.$count_date.' วัน)'; ?></div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">รายการ</th>
                            <th class="text-right">จำนวน</th>
                            <th class="text-right">ราคา</th>
                            <th class="text-right">รวม</th>
                        </tr>
                    </thead>
                    <tbody>
                        

                        <?php //debug($reservations);
                            foreach ($reservations as $reservation) 
                            {
                                if($reservation['PriceRoom']['tag'] == 1)
                                {
                        ?>
                            <tr>
                                <td class="no">1</td>
                                <td class="text-left">
                                     <h4>
                                         <?php 
                                                echo $reservation['PriceRoom']['name']; 
                                                //debug($dates);    
                                         ?>
                                     </h4>
                                </td>
                                <td class="unit">
                                    <?php 
                                                echo $reservation['Reservation']['number'].' <font style="font-size:12px">'.$reservation['PriceRoom']['unit'].'</font>';     
                                    ?>
                                </td>
                                <td class="qty">
                                    <?php 
                                                echo number_format($reservation['PriceRoom']['price']*$count_date);     
                                    ?>
                                </td>
                                <td class="total">
                                    <?php 
                                        $total_row = $reservation['Reservation']['number']*$reservation['PriceRoom']['price'];
                                        $total_all = $total_all+$total_row;
                                                echo number_format($total_row);     
                                    ?>
                                </td>
                            </tr>
                        <?php 
                                }
                            }
                        ?>



                        <?php //debug($reservations);
                            foreach ($reservations as $reservation) 
                            {
                                if($reservation['PriceRoom']['tag'] != 1)
                                {
                        ?>
                            <tr>
                                <td class="no">
                                        <?php 
                                                echo $no_i;  
                                                $no_i++;     
                                         ?>
                                </td>
                                <td class="text-left">
                                     <h4>
                                         <?php 
                                                echo $reservation['PriceRoom']['name'];     
                                         ?>
                                     </h4>
                                </td>
                                <td class="unit">
                                    <?php 
                                                echo $reservation['Reservation']['number'].' <font style="font-size:12px">'.$reservation['PriceRoom']['unit'].'</font>';     
                                    ?>
                                </td>
                                <td class="qty">
                                    <?php 
                                                echo number_format($reservation['PriceRoom']['price']);     
                                    ?>
                                </td>
                                <td class="total">
                                    <?php 
                                        $total_row = $reservation['Reservation']['number']*$reservation['PriceRoom']['price'];
                                        $total_all = $total_all+$total_row;
                                                echo number_format($total_row);     
                                    ?>
                                </td>
                            </tr>
                        <?php 
                                }
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <!--<tr>
                            <td colspan="2"></td>
                            <td colspan="2">SUBTOTAL</td>
                            <td>$5,200</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TAX 25%</td>
                            <td>$1,300</td>
                        </tr>-->

                        <?PHP 
                            function convert($number){ 
                            $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ'); 
                            $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน'); 
                            $number = str_replace(",","",$number); 
                            $number = str_replace(" ","",$number); 
                            $number = str_replace("บาท","",$number); 
                            $number = explode(".",$number); 
                            if(sizeof($number)>2){ 
                            return 'ทศนิยมหลายตัวนะจ๊ะ'; 
                            exit; 
                            } 
                            $strlen = strlen($number[0]); 
                            $convert = ''; 
                            for($i=0;$i<$strlen;$i++){ 
                                $n = substr($number[0], $i,1); 
                                if($n!=0){ 
                                    if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; } 
                                    elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; } 
                                    elseif($i==($strlen-2) AND $n==1){ $convert .= ''; } 
                                    else{ $convert .= $txtnum1[$n]; } 
                                    $convert .= $txtnum2[$strlen-$i-1]; 
                                } 
                            } 


                            return $convert; 
                            } 
                            ## วิธีใช้งาน

                            $x = $total_all; 
                            //echo  $x  . "=>" .convert($x); 
                        ?>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2" style=" padding-bottom: 24px;padding-top: 0px;">GRAND TOTAL</td>
                            <td style="padding-right: 8px;">
                                <?php echo number_format($total_all); ?>
                                <br>
                                ( <?php echo convert($x); ?>บาทถ้วน )
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <div class="thanks"></div><br><br>
                <div class="notices">

                    <?php 
                        $new_startDate2 = date("Y-m-d",strtotime($user['User']['modified']));
                        $strNewDate2 = date("Y-m-d", strtotime("+3 day", strtotime($new_startDate2)));
                        $str2=explode("-",$strNewDate2);
                        $date2 = trim($str2[2]).'/'.trim($str2[1]).'/'.trim($str2[0]); 
                        $new_startDate3 = $user['User']['modified'];
                    ?>

                    
                    <div style="font-weight: bold">NOTICE:</div>
                    <div class="notice">
                        <?php echo $user['User']['note']; ?>
                        <br>
                        <?php 
                            if($user['User']['status'] == 1){
                                $text_status = 'รอการตรวจสอบ';
                            }elseif($user['User']['status'] == 2){
                                $text_status = 'รายการของท่านอนุมัติแล้วกรุณาชำระเงินภายในวันที่ '.$date2;
                            }elseif($user['User']['status'] == 3){
                                $text_status = 'ชำระเงินเรียบร้อย';
                            }else{
                                $text_status = 'ขออภัยรายการทีท่านจองไม่ผ่านการอนุมัติ';
                            }
                            echo '<h3>'.$text_status.'</h3>';
                        ?>

                    </div>
                </div>

            </main>
            <footer>
                <!--Invoice was created on a computer and is valid without the signature and seal.-->
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>



<script type="text/javascript">
     $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
</script>