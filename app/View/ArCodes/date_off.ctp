<br>
<div class="container">
	

	<div class="col-md-12">
		<div class="alert alert-warning">
		  <strong><span class="glyphicon glyphicon-off"></span> ปิดการจอด</strong>
		</div>
	</div>

	<div class="col-md-12 text-right">
		<button class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</button>
	</div>

	<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        <form method="post">
          <p>
          	<label><span class="glyphicon glyphicon-calendar"></span> เลือก วัน/เดือน/ปี</label>
          	<input type="date" name="data[DateOff][date]" class="form-control" required="true">
          </p>
          <p>
          	<?php
					echo $this->Form->input('DateOff.room_id', array(
							'options' => $list_rooms,
							'empty' => 'เลือกสถานที่',
							'class' => 'form-control',
							'required' => true,
							'label' => false
					));
			?>
          </p>
        </div>
        <div class="modal-footer">
        	<button type="submit" class="btn btn-success" >บันทึก</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

	<div class="col-md-12"><br>
		<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>ลำดับ</td>
					<td>Date</td>
					<td>สถานที่</td>
					<td>#</td>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach ($date_offs as $date_off)
				{
					$date_off_id = $date_off['DateOff']['id'];
				?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $date_off['DateOff']['date']; ?></td>
						<td><?php echo $date_off['Room']['name']; ?></td>
						<td><a href='<?php echo $this->Html->url(array("controller" => "EBookings","action" => "delete_date_off",$date_off_id)); ?>' class="btn btn-danger btn-sm" OnClick="return chkdel();"><span class="glyphicon glyphicon-trash"></span> ลบ</a></td>
					</tr>
				<?php
				$i++;
				}
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-12">
		
	</div>


</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>


<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
}
</script>