<br>
<div class="container">
	

	<div class="col-md-12">
		<div class="alert alert-success">
		  <strong><span class="glyphicon glyphicon-th-list"></span> รายการจอง</strong>
		</div>
	</div>

	<div class="col-md-12"><br>
		<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>ลำดับ</td>
					<td>นามผู้จอง</td>
					<td>สถานที่</td>
					<td>วันที่ใช้บริการ</td>
					<td>สถานะ</td>
					<td>#</td>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach ($users as $user)
				{
					$user_id = $user['User']['id'];
				?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $user['User']['name']; ?></td>
						<td><?php echo $user['Room']['name']; ?></td>
						<td><?php echo $user['User']['date_range']; ?></td>
						<td>
							<form method="post">
							<input type="hidden" name="data[User][id]" value="<?php echo $user['User']['id']; ?>">
							<?php echo $this->Form->input('User.status', 
                                    array('options' => array(
                                      '1' => 'รอการตรวจสอบ ', 
                                      '2' => 'อนุมัติ',
                                      '3' => 'ชำระเงินแล้ว',
                                      '0' => 'ไม่อนุมัติ'), 
                                      'legend' => false,
                                      'value' => $user['User']['status'],
                                      'class' => 'form-control',
                                      'separator' => '<br/>',
                                      //'onchange' => 'this.form.submit()',
                                      //'OnClick' => 'return chkdel()',
                                      'required',
                                      'label' => false,    
                                    ));
                            ?>
                            <button type="submit" class="btn btn-success btn-sm" OnClick="return chkdel()">บันทึก</button>
                            </form>
						</td>
						<td>
							<a href='<?php echo $this->Html->url(array("controller" => "EBookings","action" => "reservation",$user['User']['code'])); ?>' class="btn btn-primary  btn-sm"><span class="glyphicon glyphicon-log-in"></span> รายละเอียด</a>
						</td>
					</tr>
				<?php
				$i++;
				}
				?>
			</tbody>
		</table>
	</div>

</div>



<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>


<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
}
</script>