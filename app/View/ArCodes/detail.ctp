<br>
<div class="container">

	<div class="col-md-12">
		<div class="alert alert-success">
		  <strong><?php echo $room['MaehiaEbookingRoom']['name']; ?></strong>
		</div>
	</div>
	
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="col-md-12">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
				      <img src="http://www.agri.cmu.ac.th/2017/files/AgriPersonal/514/514df_20180920_110410.jpg" alt="Los Angeles">
				    </div>

				    <div class="item">
				      <img src="http://www.agri.cmu.ac.th/2017/files/AgriPersonal/514/Slide3.JPG" alt="Chicago">
				    </div>

				  </div>

				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<div class="col-md-12">
				<br>
				<label><i class="fa fa-tasks" aria-hidden="true"></i> รายละเอียดเพิ่มเติม</label><br>
				&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $room['MaehiaEbookingRoom']['detail']; ?>
			</div>
		</div>

		<div class="col-md-6">
			<form method="post" action="<?php  //echo $this->Html->url(array("controller" => "EBookings","action" => "add_reservation")); ?>">
				<label><i class="fa fa-calendar" aria-hidden="true"></i> เลือกวันที่พัก <a href="<?php echo $this->Html->url(array("controller" => "EBookings","action" => "detail",$room_id,"th")); ?>">TH</a>/<a href="<?php echo $this->Html->url(array("controller" => "EBookings","action" => "detail",$room_id,"en")); ?>">EN</a></label>
				<div class="col-md-12">
					

					<div class="col-md-6">
						<label><i class="fa fa-calendar" aria-hidden="true"></i> เริ่ม</label>
						<?
						if($local == 'en')
						{
						?>
						<input name="data[MaehiaEbookingReservation][start_date]" value="" type="text" data-provide="datepicker" autocomplete="off" class="datepickerthai form-control" data-date-language="en-en" pattern="\d{1,2}/\d{1,2}/\d{4}"  required="true">
						<?
						}else{
						?>
						<input name="data[MaehiaEbookingReservation][start_date]" value="" type="text" data-provide="datepicker" data-date-language="th-th" autocomplete="off" class="datepickerthai form-control" pattern="\d{1,2}/\d{1,2}/\d{4}"  required="true">
						<?
						}
						?>
					</div>

					<div class="col-md-6">
						<label><i class="fa fa-calendar" aria-hidden="true"></i> สิ้นสุด</label>
						<?
						if($local == 'en')
						{
						?>
						<input name="data[MaehiaEbookingReservation][end_date]" value="" type="text" data-provide="datepicker" autocomplete="off" class="datepickerthai form-control datepicker" data-date-language="en-en" pattern="\d{1,2}/\d{1,2}/\d{4}"  required="true" id="datepicker">
						<?
						}else{
						?>
						<input name="data[MaehiaEbookingReservation][end_date]" value="" type="text" data-provide="datepicker" data-date-language="th-th" autocomplete="off" class="datepickerthai datepicker form-control"  required="true"  id="datepicker">
						<?
						}
						?>


					
					</div>


					
				</div>

				<div class="col-md-12">&nbsp;</div>

				<div class="col-md-12">
					<label><i class="fa fa-bed" aria-hidden="true"></i> ประเภทผู้เข้าพัก</label>

							<?php 
								$price_rooms = $room['MaehiaEbookingPriceRoom'];
								foreach ($price_rooms as $price_room) 
								{
									if($room['MaehiaEbookingRoom']['maehia_ebooking_type_room_id'] == 2)
									{
							?>
										<div class="col-md-12"  style="padding-bottom: 5px;">
											<div class="col-md-6">
												<i class="fa fa-tags" aria-hidden="true"></i> <?php echo $price_room['name'].' <br><font style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$price_room['price'].' บาท</font>'; ?>	
											</div>
											<div class="col-md-6">

												<div class="col-md-6">
													<input type="hidden" class="form-control" name="data[Radio][disabled]" value="1">	
													<input type="radio" id="radio1" data-id="<?php echo $price_room['id']; ?>" name="data[Radio][number]" value="<?php echo $price_room['id']; ?>" required>	
												</div>
												<div class="col-md-6">
													<?php echo $price_room['unit']; ?>	
												</div>
												
											</div>
										</div>	
							<?php 	}elseif($room['MaehiaEbookingRoom']['maehia_ebooking_type_room_id'] == 3)
									{
							?>	

									<div class="col-md-12"  style="padding-bottom: 5px;">
										<div class="col-md-6">
											<?php echo $price_room['name']; ?>	
										</div>
										

										<div class="col-md-6">
											<?php echo $price_room['price']; ?>	
											บาท/<?php echo $price_room['unit']; ?>	
										</div>
											
										
									</div>	





							<?php
									}else{


							
							?>
										<div class="col-md-12"  style="padding-bottom: 5px;">
											<div class="col-md-6">
												<i class="fa fa-tags" aria-hidden="true"></i> <?php echo $price_room['name'].' <br><font style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$price_room['price'].' บาท</font>'; ?>	
											</div>
											<div class="col-md-6">

												<div class="col-md-6">
													<input type="hidden" class="form-control" name="data[MaehiaEbookingReservation][maehia_ebooking_price_room_id][]" value="<?php echo $price_room['id']; ?>">	
													<input type="number" class="form-control" name="data[MaehiaEbookingReservation][count_preson][]" min="0" max="" value="0" required>	
												</div>
												<div class="col-md-6">
													<?php echo $price_room['unit']; ?>	
												</div>
												
											</div>
										</div>	

							<?php									
									}
								}
							?>
					
				</div>

				<input type="hidden" class="form-control" name="data[MaehiaEbookingReservation][maehia_ebooking_status_order_id]" value="1" required>

				<input type="hidden" class="form-control" name="data[MaehiaEbookingReservation][maehia_ebooking_type_room_id]" value="<?=$room['MaehiaEbookingRoom']['maehia_ebooking_type_room_id']?>" required>		

			<!--
				<div class="col-md-12" style="padding-bottom: 5px;">
					<label><i class="fa fa-id-card-o" aria-hidden="true"></i> นามผู้จอง <font style="color: red">*</font></label>
					<input type="text" class="form-control" name="data[User][name]" required>
				</div>

				<div class="col-md-6" style="padding-bottom: 5px;">
					<label><i class="fa fa-mobile" aria-hidden="true"></i> เบอร์โทรศัพท์ <font style="color: red">*</font></label>
					<input type="text" class="form-control" name="data[User][tel]" required>
				</div>

				<div class="col-md-12" style="padding-bottom: 5px;">
					<label><i class="fa fa-envelope" aria-hidden="true"></i> อีเมล์ </label> <font style="color: red"> *โปรดกรอกอีเมล์ที่ใช้งานได้จริงเพื่อทำการยืนยันการจอง</font>
					<input type="email" class="form-control" name="data[User][email]" required>
				</div>

				<div class="col-md-12" style="padding-bottom: 5px;">
					<label><i class="fa fa-inbox" aria-hidden="true"></i> หมายเหตุ</label>
					<textarea class="form-control" name="data[User][note]"></textarea>
				</div>

				<div class="col-md-12" style="padding-bottom: 5px;">
					<input type="hidden" class="form-control" name="data[User][room_id]" value="<?php echo $room_id; ?>">
				</div>
			-->
				<div class="col-md-12 text-right" style="padding-bottom: 5px;">
					<br>
					<button type="submit" class="btn btn-success btn-block">ตกลง</button>
				</div>
			
			</form>
		</div>

	</div>

</div>

<?php
	if(isset($offs)){
		$set_date_offs = '';
		foreach ($offs as $off) 
		{
			$set_date_offs .= " || date.format('DD/MM/YYYY') == ".'"'.$off.'"';
		}
	}
	//echo $set_date_offs;
?>

<!-- DATERANGE -->
<?php $date = date('d/m/Y'); ?>
<script>
var date_array = ['24/11/2018', '29/11/2018'];
$(function() {
  $('input[name="data[DateRange][date]"]').daterangepicker({
    locale: {
      format: 'DD/MM/YYYY'
    },
     minDate: '<?php echo date('d/m/Y'); ?>',
     isInvalidDate: function(date) {
	    if (date.format('DD-MM-YYYY') == '1-11-2018'<?php
	if(isset($offs)){
		$set_date_offs = '';
		foreach ($offs as $off) 
		{
			$set_date_offs .= " || date.format('DD/MM/YYYY') == ".'"'.$off.'"';
		}
	}
	echo $set_date_offs;
?>) 
	    {
		    return true; 
		}else{
		    return false; 
		}
	}
  });
});
</script>

<!-- DATERANGE -->




