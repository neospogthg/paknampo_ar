<div class="container">
	<div class="col-md-12">
		<div class="container">
	
			<h1>สมัครสมาชิก</h1>
		
			<div class="row">		
					<?php echo $this->Form->create('User',array('class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>
			
					<div class="form-group">
						<label class="col-sm-2 control-label"><font color="red">* </font>Username:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('username', array('label' => false,'class' => 'form-control','required' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	  
					<div class="form-group">
						<label class="col-sm-2 control-label"><font color="red">* </font>Password:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('password', array('label' => false,'class' => 'form-control','required' => true,'type'=>'password',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	
					
					<!--div class="form-group">
						<label class="col-sm-2 control-label">เลขที่ประจำตัวประชาชน:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('personal_id', array('type'=>'text','maxlength'=>'13','size'=>'13','label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
							  
					<div class="form-group">
						<label class="col-sm-2 control-label"><font color="red">* </font>คำนำหน้า:</label>
						<div class="col-sm-4">

							<?php 
							echo $this->Form->input('prefix_id', array(
								'options' => $prefixs,
								'class' => 'form-control',
								'label' => false,
								'required' => true
							));				
							?>
						</div>
					</div-->
					
					<div class="form-group">
						<label class="col-sm-2 control-label"><font color="red">* </font>ชื่อ - นามสกุล:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('firstname', array('label' => false,'class' => 'form-control','required' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
					<!--div class="form-group">
						<label class="col-sm-2 control-label"><font color="red">* </font>นามสกุล:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('lastname', array('label' => false,'class' => 'form-control','required' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-sm-2 control-label">วันเกิด:</label>
						<div class="col-sm-4">							
								<?php echo $this->Form->date('birthdate', 
									array('type'=>'text',
									'class' => 'datepickerthai form-control',
									'data-provide' => 'datepicker',
									'data-date-language' => 'th-th',
									'pattern' => '\d{1,2}/\d{1,2}/\d{4}')); ?>	
						</div>
					</div-->
					
					<div class="form-group">
						<label class="col-sm-2 control-label">อีเมล์:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('email', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					
					<div class="form-group">
						<label class="col-sm-2 control-label">โทรศัพท์:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('phone', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
					
					<!--div class="form-group">
						<label class="col-sm-2 control-label">facebook:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('facebook', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">line:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('line', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">instagram:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('ig', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">ที่อยู่:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('address', array('label' => false,'class' => 'form-control','type' => 'text-area',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">รูป:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('photo_tmp',array('type'=>'file','label' => false));?>				
						</div>
					</div-->

					
					<?php echo $this->Form->input('active',array('type'=>'hidden','value'=>'1')); ?>
					<?php echo $this->Form->input('user_type_id',array('type'=>'hidden','value'=>'3')); ?>
					
				</div>
				
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
							<div class="form-group ">								
								<button type="submit" class="btn btn-blue btn-lg btn-block">สมัครสมาชิก</button>		  
								<a href="javascript:history.go(-1)" class="btn btn-blue btn-lg btn-block">ยกเลิก</a>
							</div>
					</div>
				</div>
				
		</div>
	</div>
</div>