<?php
$strCurrentURL = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//$my_img = 'https://pbs.twimg.com/profile_images/1086973305733566464/Cn25L8Qd_400x400.jpg';

/*$mypoint_photo = 'photo'.$checkin_id;
$my_img = $this->webroot.'img/checkin_photo'.$checkin_id.'/'.$cUserId['User'][$mypoint_photo];*/

$my_img = $this->webroot.'img/checkin_photo'.$checkin_id.'/'.$checkinPhoto['CheckinPhoto']['photo'];
?>
<html>
<head>
  <title>Paknampho Chinese New Year</title>
  <meta property="og:url" content="<?=$my_img;?>" />
  <meta property="og:type" content="Paknampho Chinese New Year" />
  <meta property="og:title" content="ตรุษจีนปากน้ำโพ | Paknampho Chinese New Year" />
  <meta property="og:description" content="รู้จักกับ งานประเพณีแห่เจ้าพ่อเจ้า-แม่ปากน้ำโพ  ครั้งเมื่อเกิดอหิวาตกโรคและฝีดาษระบาดครั้งใหญ่ เมื่อ 100 กว่าปีที่ผ่านมา ทำให้ชาวปากน้ำโพคนล้มตายเป็นจำนวนมาก ซึ่งในสมัยนั้นวิทยาการทางการแพทย์ยังไม่แพร่หลาย ผู้คนต่างก็อาศัยศรัทธาที่มีต่อองค์เจ้าพ่อ-เจ้าแม่ มีการทำ “พิธีเผาฮู้” แล้วนำไปผสมน้ำไว้ให้ผู้คนได้ดื่มกิน ปรากฏเป็นสิ่งมหัศจรรย์ โรคร้ายนั้นเริ่มทุเลาหายไป และผู้คนกลับมามีสุขภาพที่แข็งแรง อีกครั้ง จึงทำให้เกิดศรัทธาอันแรงกล้าและเป็นที่มาของการฉลองสมโภช นำองค์เจ้าพ่อ-เจ้าแม่ออกแห่เพื่อเป็นสิริมงคลต่อชีวิต ครอบครัว และชาวเมืองปากน้ำโพ นับแต่นั้นสืบมาในงานประเพณีแห่เจ้าพ่อ-เจ้าแม่ปากน้ำโพ ในสมัยแรกๆนั้น มีวัตถุประสงค์ในการนำองค์เจ้าพ่อ-เจ้าแม่ออกแห่ เพื่อให้ประชาชนทั่วทุกหัวระแหงได้มาสักการะบูชากราบไหว้เพื่อเป็นสิริมงคล โดยมีการแสดงเล็กๆ น้อยๆ ที่ได้รับอิทธิพลมาจากวัฒนธรรมจีนออกแห่แสดงในขบวนด้วย เมื่อกาลเวลาผ่านพ้นไป ประเพณีฯเองนั้นจัดขึ้นในช่วงเวลาของการเฉลิมฉลองวันขึ้นปีใหม่จีน หรือวันตรุษจีน จึงได้เกิดเป็นขบวนแห่ขบวนต่างๆ รวมเข้าไปอยู่ในการแห่เจ้าพ่อเจ้า-แม่เพิ่มขึ้น เช่น การแห่มังกร สิงโต ขบวนนางฟ้า ขบวนเด็กรำถ้วย เอ็งกอพะบู๊ และขบวนองค์สมมติ เจ้าแม่กวนอิม เป็นต้น ทั้งนี้เพื่อให้ชาวปากน้ำโพเกิดความรู้สึกทั้งความเป็นสิริมงคล และเป็นการเฉลิมฉลองปีใหม่ ซึ่งเป็นช่วงเวลาที่ชาวจีนจัดให้มีการไหว้ในวันส่งท้ายปี และการเที่ยวในวันขึ้นปีใหม่ในช่วงเทศกาลตรุษจีนอยู่แล้ว" />
  <meta property="og:image" content="<?=$my_img;?>" />
</head>
<body>
  <div class="container">



          <div class="col-md-12">
            <label><h2>จุด Check In <?=$checkin_name;?></h2></label>
              <img src="<?=$my_img;?>" style="width:100%;">
              <br><br>
          </div>

          <center>
          <div id="fb-root"></div>
          <br><br>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>

          <div class="fb-share-button" 
            data-href="<?=$my_img;?>" 
            data-layout="button_count" style="width: 300px">
          </div></center>
  </div>        
  <br><br>
</body>
</html>