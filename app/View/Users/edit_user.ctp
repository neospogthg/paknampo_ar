<div class="container">
	<div class="col-md-12">
		<div class="container">
		
			<?php echo $this->Form->create('User',array('class'=>'form-horizontal','enctype'=>'multipart/form-data','method'=>'post'));?>
		
			
			
			<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$this->request->data['User']['id'])); ?>
			<?php echo $this->Form->input('facebook_id',array('type'=>'hidden','value'=>$this->request->data['User']['facebook_id'])); ?>
			
			<div class="row">

					<!--div class="form-group">
						<div class="col-md-12 text-center" style="font-size: 40px;">
						แก้ไขข้อมูลส่วนตัว
						</div>
					</div-->
									
					<div class="form-group">
						<label class="col-sm-4 control-label">Picture:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('picture', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						 <img src="<?php echo $this->request->data['User']['picture']; ?>">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Facebook ID:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('facebook_id', array('label' => false,'class' => 'form-control','readonly' => true,'type' => 'text',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Name:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('name', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Email:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('email', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	

					<div class="form-group">
						<label class="col-sm-4 control-label">Link:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('link', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Phone:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('phone', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
					
					
					<?php echo $this->Form->input('active',array('type'=>'hidden','value'=>$this->request->data['User']['active'])); ?>			
			</div>
				
			<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-blue btn-lg btn-block">บันทึก</button>	
								<!--a href="javascript:history.go(-1)" class="btn btn-danger">ยกเลิก</a-->														
							</div>
					</div>
			</div>
				
		</div>
	</div>
</div>