<div class="container">
	<div class="col-md-12">
			
			<div class="row">	
				<?php echo $this->Form->create('User',array('class'=>'form-horizontal','enctype'=>'multipart/form-data','method'=>'post'));?>
					<div class="form-group">
						<label class="col-sm-4 control-label">Picture:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('picture', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						 <img src="<?php echo $this->request->data['User']['picture']; ?>">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Facebook ID:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('facebook_id', array('label' => false,'class' => 'form-control','readonly' => true,'type' => 'text',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-sm-4 control-label">Name:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('name', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Email:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('email', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>	

					<div class="form-group">
						<label class="col-sm-4 control-label">Link:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('link', array('label' => false,'class' => 'form-control','readonly' => true,
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Phone:</label>
						<div class="col-sm-4">
						<?php echo $this->Form->input('phone', array('label' => false,'class' => 'form-control',
																		'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
													))); ?>
						</div>
					</div>
						
			</div>
			
	<b>
<?php
		foreach ($checkinPhotos as $checkinPhoto) 
		{
			if($checkinPhoto['CheckinPhoto']['photo'] != null)
			{
				$img = $this->webroot.'img/checkin_photo'.$checkinPhoto['CheckinPhoto']['checkin_id'].'/'.$checkinPhoto['CheckinPhoto']['photo'];
				echo("Check-in Point ".$checkinPhoto['CheckinPhoto']['checkin_id']." ".$checkinPhoto['CheckinPhoto']['created']);
?>
				<img src="<?php echo $img; ?>" style="width:200px;">
				<script>(function(d, s, id) 
				{
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>

				<div class="fb-share-button" 
					data-href="<?php echo $img;?>" 
					data-layout="button_count" style="width: 300px">
				</div>
				</br>
<?php				
			}
		}
?>
	</b>
	</div>
</div>
