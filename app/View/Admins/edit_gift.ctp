<div class="container">
    <div class="col-md-12">
		<div class="container">

			<h1>แก้ไขข้อมูลของรางวัล</h1>

			<div class="row">
				<?php echo $this->Form->create('Gift',array('class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>
				<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$this->request->data['Gift']['id'])); ?>
				
				 <div class="col-md-12">
					<div class="card">
						<div class="card-header">					

							<div class="form-group">
								<label class="col-sm-2 control-label">ชื่อ:</label>
								<div class="col-sm-4">
									<?php echo $this->Form->input('name', array('label' => false,'class' => 'form-control','type' => 'text',
																				'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
															))); ?>
								</div>	
							</div>
							
														<div class="form-group">
								<label class="col-sm-2 control-label">รายละเอียด:</label>
								<div class="col-sm-4">
									<?php echo $this->Form->input('description', array('label' => false,'class' => 'form-control','type' => 'textarea',
																				'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
															))); ?>
								</div>	
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">ประเภทของรางวัล:</label>
								<div class="col-sm-4">

									<?php 
									echo $this->Form->input('gift_type_id', array(
										'options' => $giftTypes,
										'class' => 'form-control',
										'label' => false,
										'required' => true
									));				
									?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">จำนวน:</label>
								<div class="col-sm-4">
								<?php echo $this->Form->input('qty', array('label' => false,'class' => 'form-control','type'=>'number','value'=>$this->request->data['Gift']['qty'],
																				'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
															))); ?>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">รูป:</label>
								<div class="col-sm-4">
								<img src="<?php $photo = $this->webroot . 'img/gift_pic/'.$this->request->data['Gift']['photo']; 
												echo $photo; ?>" alt="รูป" style="width:100px;height:100px;"/>
												
								<?php echo $this->Form->input('photo_tmp',array('type'=>'file','label' => false));?>				
								</div>
							</div>
								
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="col-sm-4">
										<div class="form-group ">								
											<button type="submit" class="btn btn-blue btn-lg btn-block">บันทึก</button>		  
											<!-- a href="javascript:history.go(-1)" class="btn btn-blue btn-lg btn-block">ยกเลิก</a-->
										</div>
								</div>
							</div>
							
						</div>						
					</div>				
				</div>
			</div>
			
						
		</div>
	</div>
</div>
