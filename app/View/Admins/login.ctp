
<div class="container">
	<div class="wrapper">
		<form class="form-signin" method="post">		
			<div style="text-align: center;">
				<img src="<?php echo $this->Html->url('/img/Asset 5@10x.png'); ?>" class="img-responsive" style="display: block; width:50%; margin: auto;"/>
			</div>
			<hr class="colorgraph"><br>
			<div class="col-md-12">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<div class="form-group">			
						<input type="text" name="data[UserName]" class="form-control" placeholder="UserName" required autofocus>	
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div>	
			
			<div class="col-md-12">
				<div class="col-md-4">
				</div>				
				<div class="col-md-4">
					<div class="form-group">				
						<input type="password" name="data[Password]" class="form-control" placeholder="Password" required>
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="col-md-4">
				</div>				
				<div class="col-md-4">
					<div class="form-group">				
						<button class="btn btn-blue btn-lg btn-block" type="submit">Login</button>
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div>
			
		</form>
	</div>
</div> <!-- /container -->