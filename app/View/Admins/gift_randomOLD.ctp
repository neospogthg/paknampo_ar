<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lucky Gift</title>

  <?php echo $this->Html->css('/assets/Animate-Text-With-Random-Characters-jQuery-Descrambler/dist/roboto+condensed.css'); ?>

  <?php echo $this->Html->script('/assets/Animate-Text-With-Random-Characters-jQuery-Descrambler/dist/jquery-1.12.4.min.js'); ?>

  <?php echo $this->Html->script('/assets/Animate-Text-With-Random-Characters-jQuery-Descrambler/dist/descrambler.min.js'); ?>
  <?php echo $this->Html->script('/assets/Animate-Text-With-Random-Characters-jQuery-Descrambler/main.js'); ?>

  <?php echo $this->Html->css('/assets/Animate-Text-With-Random-Characters-jQuery-Descrambler/style.css'); ?>

	<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>
	<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>
	<?php echo $this->Html->css('blue-ui');	?>
</head>
<body style="<?php echo $this->Html->style(array('margin-bottom' => '0'));?> ; background-image: url('<?php echo $this->Html->url('/img/body_03.png'); ?>');background-size: 100% 100%;background-repeat: no-repeat;margin-bottom: 1px">
<?
//debug($user);
?>
<div class="row">
      <div class="container-fulid">
        <img src="<?php echo $this->Html->url('/img/header_03.png'); ?>" style="width:100%;margin-left:-1px"/>
      </div>
    </div>  


  <div class="container">

    <div class="input-section">

      <p class="notice"></p>
    </div>

   <!-- <h2>
    <?= $giftTypes['GiftType']['name'];?> <br/><a href="<?php echo $this->Html->url(array('action' => 'gift_random',$gift_id)); ?>"> (สุ่มอีกครั้ง)</a>
    </h2>  -->


    <?
    if(count($user)>0){
    ?>
          <h2 class="scramble"> 
          <?= $user['User']['name'];?>
          </h2>



          <!--div class="row">
            <a href="<?php echo $this->Html->url(array('action' => 'confrim_gift',$user['User']['id'],$gift_id,$user['Privilege']['id'])); ?>">ยืนยัน</a>
          </div-->
          

    <?
    }else{
    ?>

          <h2 class=""> 
            ----- ไม่มีข้อมูล -----
          </h2>


    <?
    }
    ?>

<!-- วงล้อแดง -->	
<div class="loadingio-spinner-dual-ring-ccat84vqsjs">
	<div class="ldio-gwz4uopfmmc">
		<div>
		</div>
		<div>
		<div>
		</div>
		</div>
	</div>
</div>
<br/>

<!-- ลูกบอล 2 -->
<!--div class="loadingio-spinner-interwind-019f485ty6brj"><div class="ldio-i0k5q6pyqmb">
<div><div><div><div></div></div></div><div><div><div></div></div></div></div>
</div></div-->

<!-- ลูกบอล4 -->	
<div class="loadingio-spinner-ellipsis-ji4jy3j2hqg"><div class="ldio-z46zlk0aa7h">
<div></div><div></div><div></div><div></div><div></div>
</div></div>
<br/>

<?php 
			if(count($user)>0)
			{
?>
				<button class="btn btn-blue btn-lg" data-toggle="modal" data-target="#exampleModal">หยุด</button>
<?php
			}
			else
			{
?>
				<a target="" class="btn btn-blue btn-lg" href="<?php echo $this->Html->url(array('action' => 'gift_list')); ?>">กลับ</a>
<?php
			}
?>

  </div>


<!-- ลูกบอล4 -->	
<style type="text/css">
@keyframes ldio-z46zlk0aa7h {
   0% { transform: translate(12px,80px) scale(0); }
  25% { transform: translate(12px,80px) scale(0); }
  50% { transform: translate(12px,80px) scale(1); }
  75% { transform: translate(80px,80px) scale(1); }
 100% { transform: translate(148px,80px) scale(1); }
}
@keyframes ldio-z46zlk0aa7h-r {
   0% { transform: translate(148px,80px) scale(1): }
 100% { transform: translate(148px,80px) scale(0); }
}
@keyframes ldio-z46zlk0aa7h-c {
   0% { background: #fa1121 }
  25% { background: #3dd265 }
  50% { background: #f6f86a }
  75% { background: #60e9f4 }
 100% { background: #fa1121 }
}
.ldio-z46zlk0aa7h div {
  position: absolute;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  transform: translate(80px,80px) scale(1);
  background: #fa1121;
  animation: ldio-z46zlk0aa7h 1s infinite cubic-bezier(0,0.5,0.5,1);
}
.ldio-z46zlk0aa7h div:nth-child(1) {
  background: #60e9f4;
  transform: translate(148px,80px) scale(1);
  animation: ldio-z46zlk0aa7h-r 0.25s infinite cubic-bezier(0,0.5,0.5,1), ldio-z46zlk0aa7h-c 1s infinite step-start;
}.ldio-z46zlk0aa7h div:nth-child(2) {
  animation-delay: -0.25s;
  background: #fa1121;
}.ldio-z46zlk0aa7h div:nth-child(3) {
  animation-delay: -0.5s;
  background: #60e9f4;
}.ldio-z46zlk0aa7h div:nth-child(4) {
  animation-delay: -0.75s;
  background: #f6f86a;
}.ldio-z46zlk0aa7h div:nth-child(5) {
  animation-delay: -1s;
  background: #3dd265;
}
.loadingio-spinner-ellipsis-ji4jy3j2hqg {
  width: 200px;
  height: 200px;
  display: inline-block;
  overflow: hidden;
}
.ldio-z46zlk0aa7h {
  width: 100%;
  height: 100%;
  position: relative;
  transform: translateZ(0) scale(1);
  backface-visibility: hidden;
  transform-origin: 0 0; /* see note above */
}
.ldio-z46zlk0aa7h div { box-sizing: content-box; }
</style>

<!-- ลูกบอล2 -->	
<style type="text/css">
@keyframes ldio-i0k5q6pyqmb-r {
  0%, 100% { animation-timing-function: cubic-bezier(0.2 0 0.8 0.8) }
  50% { animation-timing-function: cubic-bezier(0.2 0.2 0.8 1) }
  0% { transform: rotate(0deg) }
  50% { transform: rotate(180deg) }
  100% { transform: rotate(360deg) }
}
@keyframes ldio-i0k5q6pyqmb-s {
  0%, 100% { animation-timing-function: cubic-bezier(0.2 0 0.8 0.8) }
  50% { animation-timing-function: cubic-bezier(0.2 0.2 0.8 1) }
  0% { transform: translate(-60px,-60px) scale(0) }
  50% { transform: translate(-60px,-60px) scale(1) }
  100% { transform: translate(-60px,-60px) scale(0) }
}
.ldio-i0k5q6pyqmb > div { transform: translate(0px,-30px) }
.ldio-i0k5q6pyqmb > div > div {
  animation: ldio-i0k5q6pyqmb-r 1s linear infinite;
  transform-origin: 200px 200px;
}
.ldio-i0k5q6pyqmb > div > div > div {
  position: absolute;
  transform: translate(200px, 164px);
}
.ldio-i0k5q6pyqmb > div > div > div > div {
  width: 120px;
  height: 120px;
  border-radius: 50%;
  background: #fe718d;
  animation: ldio-i0k5q6pyqmb-s 1s linear infinite;
}
.ldio-i0k5q6pyqmb > div > div:last-child {
  animation-delay: -0.5s;
}
.ldio-i0k5q6pyqmb > div > div:last-child > div > div {
  animation-delay: -0.5s;
  background: #46dff0;
}
.loadingio-spinner-interwind-019f485ty6brj {
  width: 400px;
  height: 400px;
  display: inline-block;
  overflow: hidden;
}
.ldio-i0k5q6pyqmb {
  width: 100%;
  height: 100%;
  position: relative;
  transform: translateZ(0) scale(1);
  backface-visibility: hidden;
  transform-origin: 0 0; /* see note above */
}
.ldio-i0k5q6pyqmb div { box-sizing: content-box; }
</style>

<!-- วงล้อแดง -->
<style type="text/css">
@keyframes ldio-gwz4uopfmmc {
  0% { transform: rotate(0) }
  100% { transform: rotate(360deg) }
}
.ldio-gwz4uopfmmc div { box-sizing: border-box!important }
.ldio-gwz4uopfmmc > div {
  position: absolute;
  width: 504px;
  height: 504px;
  top: 48px;
  left: 48px;
  border-radius: 50%;
  border: 48px solid #000;
  border-color: #f80233 transparent #f80233 transparent;
  animation: ldio-gwz4uopfmmc 1s linear infinite;
}
.ldio-gwz4uopfmmc > div:nth-child(2) { border-color: transparent }
.ldio-gwz4uopfmmc > div:nth-child(2) div {
  position: absolute;
  width: 100%;
  height: 100%;
  transform: rotate(45deg);
}
.ldio-gwz4uopfmmc > div:nth-child(2) div:before, .ldio-gwz4uopfmmc > div:nth-child(2) div:after { 
  content: "";
  display: block;
  position: absolute;
  width: 48px;
  height: 48px;
  top: -48px;
  left: 180px;
  background: #f80233;
  border-radius: 50%;
  box-shadow: 0 456px 0 0 #f80233;
}
.ldio-gwz4uopfmmc > div:nth-child(2) div:after { 
  left: -48px;
  top: 180px;
  box-shadow: 456px 0 0 0 #f80233;
}
.loadingio-spinner-dual-ring-ccat84vqsjs {
  width: 600px;
  height: 600px;
  display: inline-block;
  overflow: hidden;
}
.ldio-gwz4uopfmmc {
  width: 100%;
  height: 100%;
  position: relative;
  transform: translateZ(0) scale(1);
  backface-visibility: hidden;
  transform-origin: 0 0; /* see note above */
}
.ldio-gwz4uopfmmc div { box-sizing: content-box; }
</style>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <center>
      <div class="modal-header">
        <h1 class="modal-title" id="exampleModalLabel">ขอแสดงความยินดีกับ</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<h2>คุณ <?php echo($user['User']['name']);?> </h2>
      </div>
      <div class="modal-footer">  
		<div class="col-md-12">
		
		<div class="col-md-4">
		</div>	
		<div class="col-md-4">
			<a target="" type="button" class="btn btn-blue btn-lg btn-block"  href="<?php echo $this->Html->url(array('action' => 'confrim_gift',$user['User']['id'],$gift_id,$user['Privilege']['id'])); ?>">ตกลง</a>
		</div>
		<div class="col-md-4">
		</div>

      </div>
	  </center>
    </div>
  </div>
</div>

 <div class="container-fulid">

  <img src="<?php echo $this->Html->url('/img/footer_03.png');?>" style="width:100%;"/>

</div>

</body>

</html>
