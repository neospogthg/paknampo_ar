<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Lucky Draw</title>

    <meta name="Title" content="Wheel Spin Animation" />
    <meta
      name="description"
      content=""
    />
    <meta
      name="keywords"
      content=""
    />

    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, user-scalable=no"
    />

    <link rel="shortcut icon" href="icon.ico" type="image/x-icon" />

    <?php echo $this->Html->css('/assets/spin/css/wheelspin.css'); ?>
 

    <style>
      body {
        margin: 0;
        padding: 0;
      }
      #mainHolder {
        position: relative;
        width: 100%;
        margin: auto;
        background: url(<?=$this->webroot.'img/bgg.png';?>)no-repeat center center;
        background-size:100%100%;
      }
      #spinContent {
        position: absolute;
        width: 100%;
        top: 25%;
        left: 35%;
      }
      .fitImg img {
        width: 100%;
        height: auto;
        display: block;
      }

      #buttonSpin {
        position: absolute;
        width: 15%;
        top: 75%;
        left: 44%;
        cursor: pointer;
      }
      #buttonStop {
        position: absolute;
        width: 15%;
        top: 75%;
        left: 44%;
        cursor: pointer;
      }
      .wheelSpinMultipleHolder .wheelSpinBorder {
        border-left: #ffcf4b solid 5px;
		  border-right: #ffcf4b solid 5px;
	  }

	  .wheelSpinMultipleHolder .wheelSpinHighlight{
		border: #8c181a double 20px;
	  }
	  
    </style>
  </head>
  <body >
    <!-- CONTENT START-->
    <div id="mainHolder" class="fitImg" >
      <div id="buttonSpin" style="margin-top: 70px"><img src="<?=$this->webroot.'assets/spin/assets/button_spin.png';?>" /></div>
      <div id="buttonStop" style="margin-top: 70px"><img src="<?=$this->webroot.'assets/spin/assets/button_stop.png';?>" /></div>
	  
      <div id="spinContent">
        <div id="wheelSpinMoreHolder" class="wheelSpinMultipleHolder">
          <div id="wheelSpinOneHolder" class="wheelSpinHolder fitImg">
            <div class="wheelSpinList"></div>
            <div class="wheelSpinBorder"></div>
            <div class="wheelSpinHighlightBackground"></div>
            <div class="wheelSpinHighlight"></div>
            <div class="wheelSpinSelect">
              <img src="<?=$this->webroot.'assets/spin/assets/arrow.svg';?>" />
            </div>
          </div>
        </div>
      </div>
    </div>
	
    <?php echo $this->Html->script('/assets/spin/js/vendor/jquery-3.4.1.min.js'); ?>
    <?php echo $this->Html->script('/assets/spin/js/vendor/jquery-ui.min.js'); ?>
    <?php echo $this->Html->script('/assets/spin/js/wheelspin.js'); ?>

    <script>
      $(function() {
        $("#wheelSpinOneHolder").wheelSpinAnimation({
          width: 700,
          height: 300,
          prizes: [
          <?
          foreach ($user as $user) 
          {
          ?>
            {
              value: '<p style="color: #8c181a"><?php echo (str_replace("'","",$user["User"]["name"]));?></p>',
              fontSize: 50,
              lineHeight: 80
            },
          <?
          }
          ?>
          ],
          callback: wineSpinCallback
        });

        $("#wheelSpinTwoHolder").wheelSpinAnimation({
          width: 500,
          height: 250,
          prizes: [
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_blue_pattern.svg';?>",
              value: "10,000",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_orange.svg';?>",
              value: "100",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_red_pattern.svg';?>",
              value: "100,000",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_blue.svg';?>",
              value: "BETTER LUCK<br/>NEXT TIME",
              fontSize: 50,
              lineHeight: 50
            },
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_orange_pattern.svg';?>",
              value: "10",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "<?=$this->webroot.'assets/spin/assets/bg_red.svg';?>",
              value: '<img src="assets/prize_plane.svg" />'
            },
            {
              background: "assets/bg_orange_pattern.svg",
              value: '<img src="assets/prize_bike.svg" />'
            },
            {
              background: "assets/bg_red_pattern.svg",
              value: '<img src="assets/prize_tablet.svg" />'
            }
          ],
          callback: wineSpinCallback
        });

        $("#wheelSpinThreeHolder").wheelSpinAnimation({
          width: 400,
          height: 250,
          prizes: [
            {
              background: "assets/bg_blue_pattern.svg",
              value: "10,000",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "assets/bg_orange.svg",
              value: "100",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "assets/bg_red_pattern.svg",
              value: "100,000",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "assets/bg_blue.svg",
              value: "BETTER LUCK<br/>NEXT TIME",
              fontSize: 50,
              lineHeight: 50
            },
            {
              background: "assets/bg_orange_pattern.svg",
              value: "10",
              fontSize: 80,
              lineHeight: 80
            },
            {
              background: "assets/bg_orange_pattern.svg",
              value: '<img src="assets/prize_mac.svg" />'
            },
            {
              background: "assets/bg_blue_pattern.svg",
              value: '<img src="assets/prize_laptop.svg" />'
            }
          ],
          callback: wineSpinCallback
        });

        $("#buttonStop").hide();
        $("#buttonSpin").click(function() {
          $("#buttonSpin").hide();
          $("#buttonStop").show();
          $("#wheelSpinOneHolder").wheelSpinAnimation("spin");
          $("#wheelSpinTwoHolder").wheelSpinAnimation("spin");
          $("#wheelSpinThreeHolder").wheelSpinAnimation("spin");
        });

        $("#buttonStop").click(function() {
          $("#buttonStop").hide();
          $("#wheelSpinOneHolder").wheelSpinAnimation("stop");
          $("#wheelSpinTwoHolder").wheelSpinAnimation("stop");
          $("#wheelSpinThreeHolder").wheelSpinAnimation("stop");
        });

        resizeWindow();

        // $( window ).resize(function() {
        // 	resizeWindow();
        // });
      });

      function resizeWindow() {
        var windowW = window.innerWidth;
        var windowH = window.innerHeight;

        $("#mainHolder").css("width", windowW);
        $("#mainHolder").css("height", windowH);
      }

      function wineSpinCallback(data) {
        switch (data.status) {
          case "spinstart":
            //when spin start

            break;

          case "spinstop":
            //when spin stop
            $("#buttonSpin").show();
            break;

          default:
        }
      }
    </script>
  </body>
</html>
