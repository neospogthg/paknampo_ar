<div class="container">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading" style="height:80px;">
				<div class="col-md-12 text-center" style="font-size: 30px;">
					ผลการแจกรางวัล
				</div>
			</div>
			<div class="panel-body">
				<center>
					<?php echo $this->Form->create('TextGift',array('class'=>'form-horizontal','enctype'=>'multipart/form-data','method'=>'post'));?>
					<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$this->request->data["TextGift"]["id"])); ?>
					<?php echo $this->Form->input('description', array('label' => false,'class' => 'form-control','type' => 'textarea','value' =>$this->request->data["TextGift"]["description"],
																		'style' => 'font-size:18px;text-align:center;min-height:500px;',
																			'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
					))); ?>				
				
					</br>
					
					<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-4">
									<div class="form-group">
										<button type="submit" class="btn btn-blue btn-lg btn-block">บันทึก</button>	
										<!--a href="javascript:history.go(-1)" class="btn btn-danger">ยกเลิก</a-->														
									</div>
							</div>
					</div>
				</center>	
				</div>
			</div>			
		</div>
	</div>
</div>