<div class="container">
    <div class="col-md-12">
		<div class="container">

			<h1>จัดการข้อมูลผู้ใช้งาน</h1>

			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<!-- div class="pull-right">
								<a class="btn btn-blue btn-lg" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'add_user')); ?>"><i class="glyphicon glyphicon-plus"></i> เพิ่ม</a>
							</div -->
						</div>
						<div class="card-body">
							

							<table id="example" class="display" style="width:100%">
								<thead>
									<tr>
										<th style="width:1%"></th>
										<th>ชื่อ</th>
										<th>โทร</th> 										
										<th>วันที่สมัคร</th> 
										<th style="width:1%"></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$username = $this->Session->read('UserName');
									$i = 1;
									foreach ($users as $user) 
									{
									?>
									<tr>
										<td>
											
											<?php
												echo $i;
												$i++;
											?>
										</td>
										<td>
											<a href="<?php echo $this->Html->url(array('controller' => 'admins','action' => 'user_checkin',$user['User']['id'])); ?>" target="_blank">
												<?php
													echo $user['User']['name'];
												?>
											</a>
										</td>
										<td>
												<?php
													echo $user['User']['phone'];
												?>
										</td>
										<td>
												<?php
													echo $user['User']['created'];
												?>
										</td>										
										<td>
											<a href="<?php echo $this->Html->url(array('action' => 'delete_user',$user['User']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
												<i class="glyphicon glyphicon-remove"></i>
											</a>
										</td>
									</tr>
									<?
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="JavaScript">
function confirm_click(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
  }
</script>


<div class="clearfix"></div>

  <script type="text/javascript">
        $(document).ready(function() {
        $('#example').DataTable();
        } );
  </script>
