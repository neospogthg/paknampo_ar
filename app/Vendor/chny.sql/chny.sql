-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `chny`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `gifts`
-- 

CREATE TABLE `gifts` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `gift_type_id` int(11) NOT NULL,
  `qty` decimal(10,0) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- dump ตาราง `gifts`
-- 

INSERT INTO `gifts` VALUES (1, '1', '1234', 2, 4, 0, 'G1.jpg', '2019-10-26 16:08:04', '2019-10-26 16:29:58');
INSERT INTO `gifts` VALUES (2, 'testGift1', 'testGift1 detail\r\n123\r\n222', 1, 500, 1, 'G2.jpg', '2019-10-26 16:08:40', '2019-10-26 16:29:37');
INSERT INTO `gifts` VALUES (3, 'รางวัลใหญ่จ้า', 'ตุ๊กตาลูฟี่', 2, 4, 1, 'G3.jpg', '2019-10-26 16:35:34', '2019-10-26 16:35:52');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `gift_types`
-- 

CREATE TABLE `gift_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- dump ตาราง `gift_types`
-- 

INSERT INTO `gift_types` VALUES (1, 'รางวัลเล็ก');
INSERT INTO `gift_types` VALUES (2, 'รางวัลใหญ่');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `prefixes`
-- 

CREATE TABLE `prefixes` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) NOT NULL,
  `name_eng` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- dump ตาราง `prefixes`
-- 

INSERT INTO `prefixes` VALUES (1, 'นาย', 'Mr.');
INSERT INTO `prefixes` VALUES (2, 'นาง', 'Mrs.');
INSERT INTO `prefixes` VALUES (3, 'นางสาว', 'Ms.');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `users`
-- 

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `personal_id` varchar(100) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `prefix_id` int(11) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `line` varchar(100) NOT NULL,
  `ig` varchar(100) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- dump ตาราง `users`
-- 

INSERT INTO `users` VALUES (8, 'ball', '1234', '', 'supakit', 'โพธารินทร์', 1, '2019-10-26', '', '', '', '', '', 3, 'ภูฟ้าเพลส ห้อง1459 177 หมู่ 1 ถนน คันคลองชลประทาน ต.ช้างเผือก', 1, '8.jpg', '2019-10-26 18:15:13', '2019-10-26 18:15:13');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user_types`
-- 

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- dump ตาราง `user_types`
-- 

INSERT INTO `user_types` VALUES (1, 'super admin');
INSERT INTO `user_types` VALUES (2, 'admin');
INSERT INTO `user_types` VALUES (3, 'user');
