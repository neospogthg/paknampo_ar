-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Oct 16, 2019 at 11:19 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `maehia_ebookings`
-- 
CREATE DATABASE `maehia_ebookings` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `maehia_ebookings`;

-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_cookies`
-- 

CREATE TABLE `maehia_ebooking_cookies` (
  `id` int(11) NOT NULL auto_increment,
  `ip` varchar(500) collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `maehia_ebooking_cookies`
-- 

INSERT INTO `maehia_ebooking_cookies` VALUES (1, '127.0.0.1', '2019-10-16 10:03:39', '2019-10-16 10:03:39');
INSERT INTO `maehia_ebooking_cookies` VALUES (2, '127.0.0.1', '2019-10-16 10:03:43', '2019-10-16 10:03:43');

-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_date_offs`
-- 

CREATE TABLE `maehia_ebooking_date_offs` (
  `id` int(11) NOT NULL auto_increment,
  `maehia_ebooking_room_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `maehia_ebooking_date_offs`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_price_rooms`
-- 

CREATE TABLE `maehia_ebooking_price_rooms` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `unit` varchar(500) collate utf8_unicode_ci NOT NULL,
  `max` double default NULL,
  `maehia_ebooking_room_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

-- 
-- Dumping data for table `maehia_ebooking_price_rooms`
-- 

INSERT INTO `maehia_ebooking_price_rooms` VALUES (1, 'ประชาชนทั่วไป', 70, 'คน/วัน', 0, 1, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (2, 'นักศึกษา', 80, 'คน/วัน', 0, 1, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (3, 'นักเรียน', 55, 'คน/วัน', 0, 1, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (4, 'ห้องเล็ก 2 เตียง (13 ห้อง)', 550, 'ห้อง/วัน', 0, 2, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (5, 'ห้องใหญ่ 4 เตียง (2 ห้อง)', 750, 'ห้อง/วัน', 0, 3, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (6, 'ค่าบำรุง', 100, 'หลัง/วัน', 0, 4, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (7, 'ค่าบำรุง+เครื่องเสียง', 3500, 'วัน', 0, 5, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (8, 'ค่าบำรุง+เครื่องเสียง', 2800, 'ครึ่งวัน', 0, 5, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (9, 'ค่าบำรุง+เครื่องเสียง', 2500, 'บาท/วัน', 0, 6, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (10, 'ค่าบำรุง+เครื่องเสียง', 2000, 'ครึ่งวัน', 0, 6, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (11, 'ค่าบำรุง+เครื่องเสียง', 1600, 'วัน', 0, 7, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (12, 'ค่าบำรุง+เครื่องเสียง', 1280, 'ครึ่งวัน', 0, 7, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (13, 'ค่าบำรุง', 600, 'วัน', 0, 8, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (14, 'ค่าบำรุง', 1000, 'วัน', 0, 9, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (15, 'ค่าบำรุง', 1200, 'วัน', 0, 10, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (16, 'อาหารเช้า ข้าวต้ม + กาแฟ + ไมโล', 55, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (17, 'อาหารว่าง ชา + กาแฟ - ขนม', 35, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (18, 'อาหาร 2 อย่าง + ข้าว + ผลไม้หรือขนมหวาน', 65, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (19, 'อาหาร 3 อย่าง + ข้าว + ผลไม้หรือขนมหวาน', 75, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (20, 'อาหาร 4 อย่าง + ข้าว + ผลไม้หรือขนมหวาน', 85, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (21, 'อาหารกล่อง', 35, 'ท่าน/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (22, 'น้ำดื่ม ขนาด 20 ลิตร', 20, 'ถัง/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (23, 'น้ำดื่มทูเวย์', 40, 'ลัง/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (24, 'น้ำแข็ง', 35, 'กระสอบ/วัน', 0, 11, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (25, 'เสริมห้องปรับอากาศ', 50, 'คน/วัน', NULL, 2, 1);
INSERT INTO `maehia_ebooking_price_rooms` VALUES (26, 'เสริมห้องปรับอากาศ', 50, 'คน/วัน', NULL, 3, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_reservations`
-- 

CREATE TABLE `maehia_ebooking_reservations` (
  `id` int(11) NOT NULL auto_increment,
  `maehia_ebooking_user_id` int(11) NOT NULL,
  `maehia_ebooking_price_rooms` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `count_preson` int(11) default NULL,
  `maehia_ebooking_status_order_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `maehia_ebooking_reservations`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_rooms`
-- 

CREATE TABLE `maehia_ebooking_rooms` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `detail` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `type_room_id` int(11) NOT NULL,
  `photo1` varchar(500) collate utf8_unicode_ci NOT NULL,
  `photo2` varchar(500) collate utf8_unicode_ci NOT NULL,
  `photo3` varchar(500) collate utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `maehia_ebooking_rooms`
-- 

INSERT INTO `maehia_ebooking_rooms` VALUES (1, 'หอพักดาหลา/ปทุมมา ห้องธรรมดา (พัดลม)', '', 1, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (2, 'หอพักดาหลา/ปทุมมา (พร้อมเครื่องปรับอากาศ)', '', 1, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (3, 'หอพักดาหลา/ปทุมมา (พร้อมเครื่องปรับอากาศ)', '', 2, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (4, 'สถานที่กางเต็นท์', '', 3, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (5, 'ห้องประชุมใหญ “สิบสองปันนา”', '', 2, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (6, 'ห้องประชุมเล็ก “จันทน์ผา”', '', 2, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (7, 'ห้องประชุมเล็ก “ลีลาวดี”', '', 2, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (8, 'ค่าบำรุงสถานที่เพื่อประกอบอาหาร (อาคารหอประชุม)', '', 3, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (9, 'ค่าไฟฟ้าสำหรับเครื่องเสียงและอุปกรณ์ดนตรีช่วงการจัดกิจกรรมนันทนาการ', '', 3, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (10, 'ค่าบำรุงพื้นที่จัดกิจกรรมบริเวณลานหอพระพุทธ และหรือบริเวณหอพักเรือนไม้', '', 3, '', '', '', 1, 0, '0000-00-00 00:00:00');
INSERT INTO `maehia_ebooking_rooms` VALUES (11, 'บริการด้านอาหารและเครื่องดื่ม', '', 4, '', '', '', 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_status_orders`
-- 

CREATE TABLE `maehia_ebooking_status_orders` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `maehia_ebooking_status_orders`
-- 

INSERT INTO `maehia_ebooking_status_orders` VALUES (1, 'จองในระบบ');
INSERT INTO `maehia_ebooking_status_orders` VALUES (2, 'รอการอนุมัติการจอง');
INSERT INTO `maehia_ebooking_status_orders` VALUES (3, 'อนุมัติการจอง');

-- --------------------------------------------------------

-- 
-- Table structure for table `maehia_ebooking_users`
-- 

CREATE TABLE `maehia_ebooking_users` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(500) collate utf8_unicode_ci NOT NULL,
  `tel` varchar(500) collate utf8_unicode_ci NOT NULL,
  `email` varchar(500) collate utf8_unicode_ci NOT NULL,
  `note` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `maehia_ebooking_cookie_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `maehia_ebooking_users`
-- 

